\chapter{Lensing clusters:  methodology and MUSE observations}
\label{chap:lensing_sample}
\minitoc
\listoffixmechapter
\newpage


\fxnote{Define $P{\mu}$ in this chapter somewhere}

Galaxy clusters are among the most massive structures of the Universe (typically $10^{14 - 15}\si{\Msun}$). Following the prediction of  general relativity, the high concentration of mass warps the surrounding space-time and prevents the light from propagating in "straight'' lines causing the optical effects that can be seen in Fig. \ref{fig:A370_arc} or Fig. \ref{fig:muse_footprint}. In a very similar way to a simple converging lens, gravitational lensing can amplify the background and is therefore often used as gravitational telescope.


To do any quantitative measurements on the background galaxies using this technique, it is necessary to characterize precisely the deformation suffered by each of them. Such 
a task is only possible provided we are able to trace the observed photons back to the source plane, behind the gravitational lens. Inverting the trajectory of light can be done using predictions of general relativity but requires the knowledge of the distribution of mass. In perfectly symmetrical cases such as black holes, this is relatively easy as the only unknown is the mass of a punctual mass. However, in a galaxy cluster this is not the case, and to add to the complexity, about 80\% of the mass budget resides in dark matter halo(s) and is not directly observable. It is nonetheless possible to reconstruct this distribution of mass using various observational techniques.\\




The goal of the  following sections on gravitational lensing is not to give a detailed review of lensing phenomena, techniques and equations, but to give the reader the minimum required to understand the analysis developed in the following chapters. For a thorough introduction to strong lensing and mass model reconstruction, we refer the reader to the excellent (and relatively easy to read) work of G. Mahler or D. J. Lagattuta who both tackle mass modelling with MUSE data (see \citet{Mahler2018}  or his thesis manuscript which is in french, and \citet{Lagattuta2017,Lagattuta2019}). A more general review can be found in \citet{Kneib2011} or \citet{Schneider1992}. In Sect. \ref{sec:method_mass_model}, we present an overview of the method for mass modelling with gravitational lensing and introduce the \lens software. In Sect. \ref{sec:observations}, we present the observations used in this work and explain the source detection and cataloging. The large number of spectroscopic redshifts and the discovery of new multiple image systems allows to build improved mass models which are presented in Sect. \ref{sec:correcting_for_lensing}. This section also illustrates how the mass model can be used to correct the observations from the lensing effects.


We also emphasize that even though the mass modelling aspect was not part of the work done during this thesis, summarizing this process is of interest to fully understand the logic of the approach, from the initial observations with MUSE leading to improved mass models, to the final selection of the LAEs used for the computation of the LF. As a side note, this chapter illustrates very well the variety and amount of work that had to be done by other people to allow this present thesis to exist.








\section{Mass modelling methodology}
\label{sec:method_mass_model}

\subsection{Constraining the mass distribution with gravitational lensing}



There exist several ways to constrain the mass distribution of galaxy clusters with observations. It is possible to derive constraints on the DM distribution using X-rays observation of the hot gas, assuming that it traces DM \citep[see e.g.,]{Merten2015}. Another method is to use the Sunyaev-Zeldovich (SZ) effect which is the inverse Compton scattering of the CMB photons with hot electrons of the intra-cluster gas. This effect induces a frequency shift in the photons of the CMB and the strength of this signature can be used to weight galaxy clusters or simply to detect them \citep[see e.g.,][]{Planck2015I,Planck2015XXIV}. However, both methods require to make assumptions on the dynamic state of the gas within the cluster to use them as tracers of DM. As illustrated in the now famous case of the Bullet cluster shown in Fig. \ref{fig:bullet_cluster}, such assumptions cannot always be made. The Bullet cluster is in fact two colliding clusters. During this process, the hot gas of the two clusters collided and was stopped "in the middle'', whereas DM being collisionless, simply went through causing the large offset seen in Fig. \ref{fig:bullet_cluster}. This is an obvious example of the fact  that X-rays emission cannot always be used to trace DM. 


\begin{figure}
\centering
\includegraphics[width=1\hsize]{Figures/bullet_cluster.pdf}
\caption{Bullet cluster seen by HST with dark matter halo component \citep{Clowe2006} and hot gas \citep{Markevitch2006} overlaid in respectively blue and pink. It can be seen that there is a large offset between these two components showing that gas is not necessarily a good tracer of DM.}
\label{fig:bullet_cluster}
\end{figure}



Unlike the SZ effect or the X-rays emission of the gas, strong gravitational lensing offers the possibility to constraint the distribution of DM without making any assumptions on the gas. The general idea of the method is to use the background galaxies that are multiply imaged to constraint the mass distribution. These multiples images of a same background galaxies appear when the wave fronts are so deformed by gravitation that they overlap with themselves as seen in Fig. \ref{fig:wavefront}. They can be used for mass modelling following the iterative process described in a very simplistic manner below:


\begin{itemize}
\item Assume a set of multiple systems, each of the systems with their own redshift or at least their constraints on the redshift. 
  
\item Assume a somewhat realistic mass distribution using the visible baryonic matter and making a guess about the DM distribution. The mass distribution is assumed to be thin in the lens plane since the depth of the ``real'' distribution is negligible with respect to the distances between the observer and the lens or the observer and the background sources.
  
\item Lens back each image of the multiple systems to their own source plane using the current mass model. If the model is not too bad, their position should coincide or almost coincide.

\item Lens each image again, from the source to the image plane using the current mass model. If the mass model is off, the positions predicted from the back and forth through the lens will be significantly different from the observed ones. On the contrary, if the model is good, the predicted positions should once again coincide with the observed positions. The mean squared difference between each predicted  and observed position is called the RMS and is often used as the value to minimize when optimizing mass models. 

\item Improve the mass model and iterate once more.
 
\end{itemize}


\begin{figure}
\centering
\includegraphics[width=0.8\hsize]{Figures/wavefront_galaxy_cluster.pdf}
\caption{Schematic view of the wave front deformation around a galaxy cluster.\\ \small{Figure taken from \citet{Kneib2011}}}
\label{fig:wavefront}
\end{figure}



The difficulty of mass modelling resides in the large dimensionality of the problem, and the entire process can not always be completely automated. For example, as the identification of multiple images is not always straightforward, it is not uncommon to use this iterative process to test whether a given image belongs or not to a certain multiple system. If the image does indeed belong to the system, the resulting RMS should decrease. An in addition, it is also common to divide the multiple system according to the level of constraints they provide and compare the mass models derived using each set of multiple system \citep[see e.g.,][]{Mahler2018,Lagattuta2019}.\\ 


The quality of the final mass models really depends on the amount of multiple systems found: provided that enough of them are found, most of the possible degeneracies can be broken. Having secure spectroscopic redshifts (as opposed to photometric redshifts) is of great help in this process. It helps identifying the image parts of a multiple system and removes one free parameter from the problem.

For this reason, MUSE is also an ideal instrument for mass modelling. It provides a complete assessment of source within the FoV, with a high number of spectroscopic redshifts. Each time MUSE observed a lensing fields, it discovered new sets of multiple systems \citep[see e.g.][]{Richard2015,Bina2016,Caminha2016,Karman2017,Lagattuta2017,Mahler2018,Lagattuta2019}, it is therefore a great asset to produce mass models of improved quality, especially when working in parallel with deep HST observations which provide high spatial resolutions.
Other constraints beside multiple systems can be used to reconstruct the mass distribution: for example it is possible to compare the intrinsic flux of images part of the same multiple system to locally constrain the magnification and therefore the mass distribution, or  to use the velocity dispersion of resolved lensed galaxies (here again we refer the reader to the thesis work of G. Mahler). \\






In practice, there are  two families of models: parametric and non-parametric reconstructions (also called free-form). In parametric reconstructions, the overall potential is modelled as the sum of sub-potentials, and each of them is parameterized with a given position, shape, set of axis and density profile. Some parameters can be fixed and the rest is optimized. With the free form approach, no shape is assumed and the mass distribution is reconstructed within and (adaptive) grid, using various lensing constraints. As such, free-forms methods are well suited for the detection of DM halos or filaments with irregular forms.

Parametric reconstructions are useful as they greatly decrease the dimensionality of the problem: each component is simplified to a finite series of simple parameters. As a result, this approach performs better when only a few constraints are available, and for extrapolations outside of the multiple-image area where no strong lensing constraints are found. On the contrary, free-form methods require a larger number of constraints to perform well and are therefore less efficient for extrapolation. In the next section we present \lens,  the strong lensing software used to optimize the parametric mass models of this work and to compute source plane projections or individual galaxy magnification. 





A more thorough comparison of various mass modelling methods is provided in \citet{Meneghetti2017} where different teams were given simulated observations of simulated galaxy clusters to try to reconstruct the original mass distribution. To understand better the possible biases in mass modelling, the existing degeneracies and how to break them we refer the reader to the work of \citet{Priewe2017}.



\subsection{Parametric modelling with \lens}


The mass models were constructed with \lens, using the parametric approach described in \citet{Kneib1996}, \citet{Jullo2007} and \citet{Jullo2009}. This parametric approach relies on the use of analytical dark-matter (DM) halo
profiles to describe the projected 2D mass distribution of the
cluster. Two main contributions are considered by \lens: one for each large-scale structure of the cluster, and one for each massive cluster galaxy. The parameters of the individual profiles are optimized through a Monte Carlo Markov Chain (MCMC)
minimization. Lenstool aims at reducing the cumulative distance in the parameter space between the predicted position of multiple images obtained from the model, and the observed ones (see RMS introduced in previous section). In the following paragraph we describe the general method used to build parametric models with \lens.


 
Because of the large number of cluster members, the optimization of each individual galaxy-scale clump cannot be achieved in practice. Instead, a relation combining the constant mass-luminosity scaling relation described in \citet{Faber1976} and the fundamental plane of elliptical galaxies is used by \lens. This assumption allows us to reduce the dimensionality of the problem leading to more
constrained mass models (and fastest convergence), On the opposite, individual parameterization of clumps would lead to  extremely degenerate results and therefore poorly constrained mass model. The analytical profiles used are double pseudo-isothermal elliptical potentials
(dPIEs) as described in \citet{Eliasdottir2007}. The ellipticity
and position angle of each elliptical profiles are measured for
the galaxy-scale clumps with \sex from high spatial resolution images of HST. Because the Brightest Cluster Galaxies (BCGs) lie at the center of clusters, they are subjected to numerous merging processes and are not expected to follow the same light-mass scaling relation. They are modeled separately in order to not bias the
final result. In a similar way, the galaxies that are close to
the multiple images or critical lines can sometimes be manually
optimized because of the significant impact they  have on the local magnification and the local geometry of the critical lines (the theoretical lines with infinite magnification in the image plane).










\section{Observations}
\label{sec:observations}

\subsection{MUSE observations}
\label{subsec:muse_obs}

\begin{figure}[htb!]
\centering
\includegraphics[width=1\hsize]{Figures/muse_footprint.pdf}
\caption{HST colour composite image overlaid with MUSE footprint. North is up and East is to the left.}
\label{fig:muse_footprint}
\end{figure}



The sample used in this study consists of four MUSE cubes of different sizes and exposure times, covering the central regions of well-characterized lensing clusters: Abell 1689,Abell 2390, Abell 2667 and Abell 2744 (resp. A1689, A2390,
A2667 and A2744 hereafter). These four clusters already had well constrained mass models before the MUSE observations, as they benefited from previous spectroscopic observations. The reference mass models can be found in \citet{Richard2010}
(LoCuSS) for A2390 and A2667, in \citet{Limousin2007} for
A1689 and in \citet{Richard2014} for the Frontier Fields cluster A2744. These observations were obtained as part of the MUSE GTO program and commissioning run (for A1689). All the observations were conducted in the nominal WFM-NOAO mode of
MUSE. The main characteristics of the four fields are listed in Table \ref{tab:observations_description}. The geometry and limits of the four FoVs are shown on the available HST images, in Fig. \ref{fig:muse_footprint}.

\input{Tables/observations_description.tex}

\begin{figure}
\centering
\includegraphics[width=0.7\hsize]{Figures/a2744_exposure_map.pdf}
\caption{Exposure time of the MUSE mosaic of A2744 overlaid on HST F814w image. The region enclosed within white line is where multiple images are expected. \\\small{Figure taken from \citet{Mahler2018}}}
\label{fig:a2744_exp_map}
\end{figure}

\paragraph*{A1689:} Observations were already presented in \citet{Bina2016} from the first MUSE commissioning run in 2014. The total exposure was divided into six individual exposures of 1100 \si{\second}. A small linear dither pattern of 0.2\arcsec was applied between each exposure to minimize the impact of the structure of the instrument (see example of slicer pattern in Fig. \ref{fig:muse_slicers})on the final data. No rotation was applied between individual
exposures

\paragraph*{A2390, A2667 and  A2744:} The same observational strategy was used for all three cubes: the individual pointings were divided into exposures of 1800 sec. In addition to a small dither pattern of 1\arcsec, the position angle was incremented by
90\degree between each individual exposure to further minimize the striping patterns caused by the slicers of the instrument. A2744 is the only mosaic included in the present sample. The strategy was to completely cover the multiple-image area. For this cluster, the exposures of the four different FoVs are as follows : 3.5, 4, 4,
5 hours of exposure plus an additional 2 hours at the centre of
the cluster (see Fig. \ref{fig:a2744_exp_map}). For A2390 and A2667, the centre of the FoV was positioned on the central region of the cluster as shown in
Table \ref{tab:observations_description} and Fig. \ref{fig:muse_footprint}.\\


All the MUSE data were reduced using the MUSE ESO reduction pipeline \citet{Weilbacher2012,Weilbacher2014}. This pipeline includes: bias subtraction, flat-fielding, wavelength and flux calibrations, basic sky subtraction, and astrometry. The individual exposures were then assembled to form a final data cube or a mosaic. An additional sky line subtraction was performed with the
ZAP software (see Sect. \ref{sec:noise_structure} for more details on the sky-line subtraction and how this affects the data). Even though the line subtraction is improved by this process, the variance in the wavelength-layers affected by the presence of sky-lines remains higher, making the source detection of faint sources more unlikely  on these layers. 


\subsection{Complementary HST observations}


For all MUSE fields analysed in this paper, complementary deep data from HST are available. They were used to help the source detection process in the cubes but also for modelling the mass distribution of the clusters (see Sect. \ref{sec:method_mass_model}). A brief list of the ancillary HST data used for this project is presented in Table \ref{tab:available_observations}. For A1689 the data are presented in \citet{Broadhurst2005}. For A2390 and A2667, a very thorough summary of all the HST
observations available are presented in \citet{Richard2008} and
more recently in \citet{Olmstead2014} for A2390. A2744 is part
of the HFF for which the details of the observations are presented in \citet{Lotz2017}. All the raw data and individual exposures are available from the
Mikulski Archive for Space Telescopes (MAST), and the details
of the reduction are addressed in the articles cited above.

\input{Tables/available_HST_observations.tex}



\subsection{Source detection}
\label{subsec:source_detection}

As established in Sect. \ref{subsec:elines_detection}, MUSE is most efficient to detect line emitters whereas deep photometry is well suited for detection of weak continua. To build a complete catalog of the sources in a MUSE cube, we combined a continuum-guided detection strategy based on deep HST images (see Table \ref{tab:available_observations} for the available photometric data) with a blind
detection in the MUSE cubes. Many of the sources end up being
detected by both approaches and the catalogs are merged at the
end of the process to make a single master catalog. The detailed
method used for the extraction of sources in A1689 and A2744
can be found in \citet{Bina2016} and \citet{Mahler2018} respectively. The general method used for A2744 (which contains the vast majority of sources in the present sample) is summarized below and follows the general guidelines presented in Sect. \ref{sec:cross_match}.\\


The presence of diffuse Intra-Cluster Light (ICL) makes the
detection of faint sources difficult in the cluster core, in particular for multiple-images located in this area. A running median filter computed in a window of 1.3\arcsec was applied to the HST images to remove most of the ICL. The ICL-subtracted images were then weighted by their inverse variance map and combined
to make a single deep image. The final photometric detection
was performed by \sex \citep{Bertin1996} on the weighted and combined deep images.

For the blind detection on the MUSE cubes, the \muselet software was used (MUSE Line Emission Tracker \footnote{Publicly available as part of the python package MPDAF \citep{Piqueras2017}.}). This tool is based on \sex to detect
emission-line objects from MUSE cubes and its precise operation is detailed in Sect. \ref{sec:source_detection}. It produces NB images for each layer of the cubes and a \sex detection is run on each of them. At the end of the process, sources with several detections across the layers of the cube are merged together to form a single master catalog.\\

Following the master catalog, all spectra were extracted for redshift measurement. For A1689, A2390 and  A2667, the 1D spectra were extracted using a fixed 1.5\arcsec aperture. For A2744 the extraction area is based on the \sex segmentation maps obtained from the deblended photometric detections described above. At this stage, the extracted spectra are only used for the redshift determination. The precise measurement of the total line fluxes requires a specific procedure, described later 
in Sect. \ref{sec:flux_computation}. Extracted spectra were manually inspected
to identify the different emission lines and accurately measure
the redshift.


A system of confidence levels was adopted to reflect the uncertainty in the measured redshifts, following \citet{Mahler2018}. The reader can find in this paper some examples that illustrate the different cases.  All the LAEs used in this thesis belong to the confidence category 2 and 3, meaning that they all have fairly robust redshift measurement. For LAEs with a single line and no continuum detected, the wide wavelength coverage of MUSE, the absence of any other line and the
asymmetry of the line were used to validate the identification of
the \lya emission. For A1689, A2390 and A2667 most
of the background galaxies are part of multiple-image systems, and are therefore confirmed high redshift galaxies based on lensing considerations.\\







In total 247 LAEs were identified in the four fields: 17 in
A1689, 18 in A2390, 15 in A2667 and 197 in A2744. The important
difference between the number of sources found in the
different fields results from a well-understood combination of
field size, magnification regime and exposure time, as explained
in Sect. \ref{subsec:source_plane}. The complete catalog of source of A2744 can be found in \citet{Mahler2018} and an online version is available\footnote{\url{http://muse-vlt.eu/science/a2744/}}.

At this stage of the analysis, it is not possible to fully describe the final LAE sample used for computing the LAE LF as the magnification of each of these LAEs is unknown and the LAE multiply imaged by the cluster are not identified yet. Both these aspects require the use of an accurate  mass model to estimate their intrinsic properties. The characteristics of the final sample used for the LF are presented in Sect. \ref{sec:description_final_sample} where it is also compared to the sample of the MUSE HUDF (deepest MUSE blank field to date).




\section{Correcting for lensing}
\label{sec:correcting_for_lensing}





\subsection{Description of the models used}


The present MUSE survey has allowed us to improve the reference models available in previous works. Table \ref{tab:mass_models} summarizes their main characteristics. For A1689, the model used is an improvement made on the model of \citet{Limousin2007}, previously
presented in \citet{Bina2016}. For A2390, the reference
model is presented in \citet{Pello1991}, \citet{Richard2010}. For A2667, the original model was obtained by \citet{Covone2006}, and was updated in \citet{Richard2010}.  For A2744, the gold model presented in \citet{Mahler2018} is used, including
as novelty the presence of NorthGal and SouthGal, two background galaxies included in the mass model as they could have a local influence on the position and magnification of multiple images.


\input{Tables/lensing_features_table.tex}


\subsection{Image plane}
\label{subsec:image_plane}

Now that the mass models have been improved thanks to the new MUSE multiple systems and spectroscopic redshifts, the mass models can be used to "unlens'' the Universe. One of the first values of interest for this work is the magnification (or amplification) noted $\mu$. This magnification can  be empirically defined as the ratio between the observed flux and the intrinsic flux of a given source. But the original definition is the ratio between the image surface and source surface, as gravitational lensing conserves the surface brightness \citep[see][]{Schneider1992}.

With \lens and a mass model, it is possible to compute predicted magnification maps in the image plane, at a given redshift. Examples of such magnification maps at $z = 3.5$ for each cluster of our sample are provided in Fig. \ref{fig:image_mag_maps}.
The theoretical lines with infinite magnification in the image plane, called critical lines, can be seen in this figure. Two main components can be seen: the large scales high magnification lines which  are surrounding the DM halos, and the smaller scale lines surrounding the cluster galaxies. This shows the importance of properly modelling the individual galaxies in a cluster as they have an important impact on the local magnification. It also becomes visible from this figure that the gradient of the magnification field becomes much higher around the high magnification regions. This is the reason why highly magnified galaxies have a much higher uncertainty attached to their magnification: a small displacement of the critical lines can have a huge impact on their measured magnification.



This type of maps can be used to measure central magnification by simply taking the magnification value at a given sky coordinate, or to compute more complex estimates such as the flux weighted magnifications presented in Sect. \ref{sec:flux_computation}.





\begin{figure}[htb!]
\centering
\includegraphics[width=1\hsize]{Figures/image_mag_maps.pdf}
\caption{Magnification maps computed by \lens at $z = 3.5$ with the MUSE footprint in green. The spatial scale varies from one panel to the other. One MUSE FoV is $\sim \ang{;1;}$ wide and the A2744 field is a $2\times 2 $ MUSE mosaic. }
\label{fig:image_mag_maps}
\end{figure}



\subsection{Source plane projection}
\label{subsec:source_plane}

As already mentioned in the introduction of this thesis, one of the major downside of using lensing is the reduction of the explored volume of Universe. One simple way to explain this is that the volume enclosed within each pixel (in the image plane) is inversely proportional to the average magnification within this pixel. Therefore, as we get closer to the high magnification regions, the density of volume (volume per angular area) decreases. This effect is illustrated in Fig. \ref{fig:source_plane_projection} showing source plane reconstruction of the four MUSE FoVs at $z = 3.5$ combined with magnification maps. For pixels multiply imaged, only the highest magnification is shown. On the left side of the figure are also shown white light images of the A2667 and A2744 MUSE cubes to compare the area seen on the sky to the effective area probed. The same angular scale is used for all images of this figure. For A1689, A2390 and A2667, the MUSE observations are centered on the core of the clusters where the magnification is maximal (see Fig. \ref{fig:image_mag_maps}), and therefore the decrease of volume is huge and about the same order of magnitude for these three clusters. For A2744, being a $2\times 2$ MUSE mosaic, the observed area is able to reach a lower magnification regime where the dilution effect is less. For this reason, the large majority of the detected LAEs are seen in the A2744 mosaic (see LAE detected count in \ref{subsec:source_detection})


When combinning the effective area probed in the four clusters, we get an effective FoV of $1.2\arcmin \times 1.2\arcmin$, which is represented as a red square on top of the A2744 white light image. Since these observations are composed of 7 MUSE pointings (3 for A1689, A2390, A2667 and 4 for A2744), this effective area is to be compared to the $\sim 7\arcmin \times 1\arcmin$ surveyed area. \\

\begin{figure}
\centering
\includegraphics[width=1\hsize]{Figures/mag_map_source_mosaic.pdf}
\caption{MUSE white light images of A2667 and A2744 on the left, and source plane projection of all 4 MUSE FoVs combined with magnification maps on the right. All images are shown using the same angular scale.}
\label{fig:source_plane_projection}
\end{figure}



One of the main interests of these source plane reconstructions, is that it allows to have more precise estimate of the effective area or volume. While such computation can still be done in the image plane by taking into account the magnification of each pixel, doing so does not allow to account for the areas of the survey that are multiply imaged. On the contrary, when working in the source plane such biases are not present, and the computation of the effective area is as simple as counting pixels on these source plane reconstructions. In addition given how much the FoVs of the three clusters A1689, A2390 and A2667 fold over themselves (see Fig. \ref{fig:source_plane_projection}) it becomes clear that this effect would be the predominant source of error if computing their effective area or volume in the image plane.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
