\chapter{Conclusion and future prospects}
\label{chap:conclusion}
\listoffixmechapter


The main remaining questions regarding the sources of cosmic reionization can be summarized as follows:

\begin{itemize}
\item[1-] By elimination, star forming galaxies are likely to drive reionization, but this has yet to be confirmed by observations. Can star forming galaxies produce enough ionizing flux density thanks to a large (but still invisible) contribution of extremely faint galaxies, to ensure the reionization of the Universe ? 

\item[2 -] What is the exact timeline of reionization ? There are evidences of a rapid transition between $6 < z < 7$, but our observational constraints stops at $z \sim 7.5$ for now, leaving the questions of the pace and beginning of reionization open. 
  
\item[3 -] What is the shape of the very faint end of the luminosity function of star forming galaxies ? How does it evolve with redshift ? Is there a turn-over in magnitude / luminosity ? 

  
\item[4 -] Are we missing a significant portion of star forming galaxies when committing to either the LAE or the LBG selection process ? Is it possible to achieve a better empirical assessment of the star forming galaxies by understanding better the interrelation between the LAE and LBG population ? 
  
\item[5 -] Are LAEs and LBGs essentially the same population and we are just unable to see it due to the observational biases inherent to these two selection methods ? \\
\end{itemize}


% --- First article
The majority of the work done during this thesis was focused on the determination of the LAE LF behind the strong lensing clusters A1689, A2390, A2667 and A2744 observed with MUSE. The goal of this study was to set constraints on the shape of the faint end of the LAE LF and to constrain the contribution of this population to reionization.

Taking advantage of the great capabilities of the MUSE instrument and using lensing clusters as a tool to reach lower luminosities, we blindly selected a population of 156 spectroscopically identified LAEs behind four lensing clusters that have $2.9 < z < 6.7 $ and magnification corrected luminosities $39 \lesssim \log L_{\rm Ly\alpha} \lesssim 43 $.
Given the complexity in combining the spectroscopic data cubes of MUSE with gravitational lensing, and taking into account that each source needs an appropriate treatment to properly account for its magnification and representativity, the computation of the LF needed a careful implementation, including some original developments. For these needs, a specific procedure was developed, including the following new methods. First, we created a precise \vmax computation for the sources found behind lensing clusters based on the creation of 3D masks. This method allows us to precisely map the detectability of a given source in  MUSE spectroscopic cubes. These masks are then used to compute the cosmological volume in the source plane. This method could be easily adapted to be used in blank field surveys.
Second, we developed a completeness determination based on simulations using the real profile of the sources. Instead of performing a heavy parametric approach based on MC source injection and recovery simulations, which is not ideally suited for lensed galaxies, this method uses the real profile of sources to estimate their individual completeness. The method is faster, more flexible, and accounts in a better way for the specificities of individual sources, both in the spatial and spectral dimensions.

After applying this procedure to the LAE population, the Lyman-alpha LF has been built for different redshift bins using 152 of the 156 detected  LAEs. Four LAEs were removed because their contribution was not trustworthy. Because of the observational strategy, this study provides the most reliable constraints on the shape of the faint end of the LFs to date and therefore, a more precise measurement of the integrated SFRD associated with the LAE population. The results and conclusions can be summarized as follows:

\begin{itemize}
\item The LAE population found behind the four lensing clusters was split in four redshift bins: $2.9 < z < 6.7$, $2.9 < z < 4.0$, $4.0 < z < 5.9, $ and $5.0 < z < 6.7$. Because of the lensing effect, the volume of Universe probed is greatly reduced in comparison to blank field studies. The estimated average volume of Universe probed in the four redshift bins are $\sim 15\,000$ Mpc$^3$, $\sim 5\,000$ Mpc$^3$, $\sim 4\,000$ Mpc$^3$, and $\sim 5\,000$ Mpc$^3$, respectively.

\item The LAE LF was computed for the four redshift bins. By construction of the sample, the derived LFs efficiently probe the low luminosity regime and the data from this survey alone provide solid constraints on the shape of the faint end of the observed LAE LFs. No significant evolution in the shape of the LF with redshift is found using these points only. These results have to be taken with caution given the complex nature of the lensing analysis, on the one hand,
and the small effective volume probed by the current sample on the other hand. Our results argue towards a possible systematic underestimation of cosmic variance in the present and other similar studies.
  
\item A Schechter fit of the LAE LF was performed by combining the LAE LF computed in this analysis with data from previous studies to constrain the bright end.  As a result of this study, a steep slope was measured for the faint end, varying with redshift between $\alpha = -1.58^{+0.11}_{-0.11}$  at $2.9 < z < 4$ and $\alpha = -1.87^{+0.12}_{-0.12}$ at $ 5 < z < 6.7 $
  
\item The \sfrdlya values were obtained as a function of redshift by the integration of the corresponding Lyman-alpha LF and compared to the levels needed to ionize the Universe as determined in \citet{Bouwens2015a}. No assumptions were made regarding the escape fraction of the Lyman-alpha photons and the \sfrdlya  derived in this work correspond to the observed values. Because of the well-constrained LFs and a better recovery of the total flux, we estimate that the present results are more reliable than previous studies. Even though the LAE population undoubtedly contributes to a significant fraction of the total SFRD, it remains unclear whether this population alone is enough to ionize the Universe at $z \sim 6$. The results depend on the actual escape fraction of Lyman-alpha photons.

  
\item The LAEs and the LBGs have a similar level of contribution at $z \sim 6$ to the total SFRD level of the Universe. Depending on the intersection between the two populations, the union of both the LAE and LBG populations may be enough to reionize the Universe at $z \sim 6$.\\
\end{itemize}


% conclusion on the use of MUSE : more complete selection
Through this work, we have shown that the capabilities of the MUSE instrument make it an ideal tool to determine the LAE LF.
Being an IFU, MUSE allows for a blind survey of LAEs, homogeneous in redshift, with a better recovery of the total flux as compared to
classical slit facilities. The selection function is also better understood as compared to NB imaging.

About $20\%$ of the present LAE sample have no identified photometric counterpart, even on the deepest surveys to date, i.e. HFF. This is an important point to keep in mind as this is a first element of response regarding the intersection between the LAE and LBG populations. Also the extension of the method presented here to other lensing fields should make it possible to improve the determination of the Lyman-alpha LF and to make the constraints on the sources of the reionization more robust. \\



To better understand the interrelation between the LAE and LBG population, we have undertaken a simultaneous search of these two populations within a given volume. The goal of this study conducted behind the HFF cluster A2744 alone was to see whether it is possible to empirically achieve a better assessment of the SFGs by looking at the intersection of these two populations. The second objective was to further characterize the biases intrinsic to each of these selection methods to investigate whether the segregation between LAEs and LBGs is indeed just an observational bias or the result of intrinsic differences.

The study conducted on A2744 led to the following results:
\begin{itemize}
\item For UV faint galaxies (\Muv $\ge 20.25$), the fraction of LAE among SFGs increases with redshift up to $z \sim 6$ and decreases for $z \sim 6$, in agreement with previous findings \citep[see e.g.][]{ArrabalHaro2018}.
  
\item The selection of \lya emitters seems to be more effective than the LBG technique to identify intrisically UV faint galaxies (\Muv > - 17) that will be missed in deep blank field surveys. In this respect, our results are in good agreement with \citet{Maseda2018}.

\item Up to \Muv $\sim - 17$, the LBG population seems to provide a good representation of the SFGs, in particular when computing the total ionizing flux in the volume explored by current surveys.

\item There is no clear evidence, based on the present results, for an intrinsic difference on the properties of the two populations selected as LBG and/or LAE. However, further investigation will be needed to conclude. In particular, some systematic trends appear in the population selected as LBG and LAE, in the sense that the UV-brightest galaxies seem to exhibit a smaller ratio in their \rf, increasing towards the faintest luminosities This could be an indication for a different distribution of dust and stars depending on the luminosity. Measuring the UV-slopes of these galaxies could provide additional information in this respect.\\

  
\end{itemize}




% add more cubes
To continue and improve on the work done during this thesis, several paths can be followed and are summed up in the paragraphs below. The first and simplest one is to add new cubes as input for the LF computation. Additional cubes would increase the overall volume of Universe explored and in average, would tend to decrease the impact of cosmic variance on the determination of the faint end slope of the LAE LF. New cubes would also add highly magnified ($\mu \gtrsim 30$) sources to the sample. And even though these sources tend to have poorly constrained magnification and luminosity (see Sect. \ref{subsec:image_plane}), it may be possible to derive statistically relevant information for $\log L \lesssim 40.5$ (the estimated completeness limit of the LF determined in this thesis) by accumulating enough of them. Finally, if we manage to double the sample in terms of effective volume it would be possible to compute the LFs for two independent and similar data sets and therefore gain further insight on the impact of cosmic variance on the derived results.

Several additional cubes are already available through the GTO collaboration or other external collaborators and are currently being prepared for the LF computation. The results obtained from this new analysis will be the object of a future publication in preparation.\\

% using results of simulation for proper estimation of CV
Our current way of accounting for cosmic variances for each point of the LF leaves space for improvement. Using the online calculator of \citet{Trenti2008}, we assume a compact geometry for all four fields and consider the population in each individual luminosity bin of the LFs as independent. None of these two assumptions is correct strictly speaking. The ideal way to improve this aspect of the work would be to draw numerous and random FoVs in results of cosmological simulations, but in practice this is challenging for two reasons. The first one is that the effective area of each of our fields strongly varies depending on the magnification/luminosity considered, and this effect is non trivial to reproduce in simulated FoVs. The second is that the low luminosity of our LAEs implies halo masses that are reaching below the resolution limits of current semi-analytic models (T. Garel, private communication), making them unreliable for our needs.\\


% use blank fields with same method
A last possibility regarding the LAE LF would be to  use the exact same method as the one presented in Chapter \ref{chap:computing_lf} to compute the LAE LF using the deep MUSE blanks fields surveys (mainly the HUDF) considering them as a limit of the strong lensing case (i.e $\mu = 1$ over the  entire FoV). Comparing  the results obtained this way to the LF computed using a "normal'' parametric selection function \citep[i.e.][]{Drake2017b,Herenz2019} would allow to further test our method and possibly improve the way we account for highly magnified objects by tuning, for example, the limit magnification $\mu_{\rm lim}$. In addition, it would allow to have a single determination of the LF over a wider range of luminosity and  allow for a more relevant optimization of a Schechter function without the need to use external data sets to constrain the bright end.\\


% intersection LAE LBGs
Finally, to complete the work done on the intersection between the LAE and LBG population behind the A2744 cluster, the same analysis could be carried out in the background of the A370 cluster which has a deep MUSE coverage with an excellent mass model \citep{Lagattuta2019} and deep photometry as part of the HFF program. The increased highly magnified volume probed this way would make our conclusions at low \lya luminosity and/or faint UV magnitudes more relevant.\\




% future instruments


In the near future, two new telescopes will allow to develop further our understanding of the reionization era and of the first galaxies: the James Webb Space Telescope (JWST) and the Extremely Large Telescope (ELT) which are scheduled for respectively 2021 and 2025.


The JWST is a space based infrared and near infrared observatory with a mirror of effective diameter of 6.5m. It was designed to serve as a successor for the Hubble Space Telescope and has both imaging and spectroscopic capabilities. The combination of the large collecting area with infrared wavelength coverage makes it well suited to study the high redshift Universe. It is expected to detect the first galaxies in the redshift range $7 \lesssim z \lesssim 15$ based on either the detection of the \lya emission with spectroscopy or the detection of a Gunn-Peterson Trough with deep photometry.


The ELT is an earth based telescope which will be equipped with a 39m main mirror and instruments covering the visible and near infrared. This unprecedented light collecting area will offer new possibilities for high redshift, large area and extremely deep cosmological surveys. It offers a strong synergy with the JWST observations because of its ability to perform large scale spectroscopic follow up observations of galaxies identified with JWST imaging, in a very similar domain of redshift. And in addition, its increased collecting area offers a higher sensitivity for detailed spectroscopic analysis, especially for continuum galaxies or galaxies with absorption lines.\\

Finally, and to finish this manuscript, it is worth underlying that even in the era of the ELT, the use of strong lensing clusters as gravitational telescopes has a promising future. It has been shown that the efficient IFU technology pioneered with MUSE had greatly improved the quality of the mass model reconstruction and therefore our abilities to use galaxy clusters to see fainter and further. As more instruments of this kind are being developed (e.g. MOSAIC and HARMONI for the ELT) the methodology introduced in this thesis could be looked upon as a pioneering approach to exploit the combination of IFU data and lensing clusters to better understand the process of galaxy formation. Because the depth of the observations evolves roughly as the square root of the exposure time, increasing the depth of observations by a factor of two requires four times more exposure. In this regards, and because telescope time is expensive, observations of strong lensing clusters remain very competitive and efficient as the depth is instantaneously increased at no additional cost other than the complexity of the data processing. The HFF program designed to push the limits of the HST and solely dedicated to observations of strong lensing clusters illustrates perfectly this last point.













%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
