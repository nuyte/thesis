import shutil as sh
import PyPDF2
import argparse




parser = argparse.ArgumentParser(description='Some options for the program.')

parser.add_argument('--paper_version','-pv',action='store_true',default=False,\
                    help='to apply the materwark only on pages with even numbers.'+\
                    'Best to use for paper version of the thesis.')

args = parser.parse_args()







thesis     = open('thesis.pdf', 'rb')
pdfReader  = PyPDF2.PdfFileReader(thesis)
outlines   = pdfReader.outlines  # contains TOC and bookmarks
first_page = pdfReader.getPage(0)


watermark = []
for i in range(10) :

    watermark_name     = 'Animation/anim_'+str(i)+'.pdf'
    pdfWatermarkReader = PyPDF2.PdfFileReader(open(watermark_name, 'rb'))
    watermark         += [pdfWatermarkReader.getPage(0)]


pdfWriter = PyPDF2.PdfFileWriter()
pdfWriter.addPage(first_page)


j = 0 # for watermark selection, different than the page number
for i,pageNum in enumerate(range(1, pdfReader.numPages)):
    

    pageObj = pdfReader.getPage(pageNum)

    if args.paper_version :
       
        if i%2 != 1 :
             # if page is not even, add without watermark
             pdfWriter.addPage(pageObj)
             continue 

    if args.paper_version :
        # in case of paper version, the animation can only be done in "reverse",
        # i.e. starting from the end of the thesis
        
        good_watermark = watermark[j%len(watermark)]
    else : 
        good_watermark = watermark[9 - j%len(watermark)]
        

        
    pageObj.mergeRotatedScaledTranslatedPage(good_watermark,0,0.20,530,10)
    
    pdfWriter.addPage(pageObj)
    j += 1
    
resultPdfFile = open('watermarkedCover.pdf', 'wb')
pdfWriter.write(resultPdfFile)


thesis.close()
resultPdfFile.close()

sh.move('watermarkedCover.pdf', 'thesis.pdf')
