\begin{otherlanguage}{french}
\chapter*{Conclusion et perspectives futures}
\label{chap:conclsuion_fr}
\addstarredchapter{Conclusion et perspectives futures}

\listoffixmechapter


Les principales questions présentées dans l'introduction de ce manuscrit sont les suivantes :

\begin{itemize}
\item[1-] Par élimination, les galaxies à formation d'étoiles sont susceptibles d'entraîner la réionisation, mais cela reste encore à confirmer par des observations. Les galaxies à formation d'étoiles peuvent-elles produire suffisamment de densité de flux UV grâce à une contribution importante (mais généralement invisible) de galaxies extrêmement faibles, pour assurer la réionisation de l'Univers ? 

\item[2 -] Quelle est la chronologie exacte de la réionisation ? Il y a des preuves d'une transition rapide entre $6 < z < 7$, mais nos contraintes observationnelles s'arrêtent à $z \sim 7.5$ pour le moment, laissant la question de l'époque du  début de la réionisation ainsi que de la vitesse de transition ouverte. 
  
\item[3 -] À quoi ressemble la partie à très faible luminosité de la fonction de luminosité des galaxies à formation d'étoiles ? Est ce que cette partie de lafonction de luminosité évolue avec le redshift ? Existe-t-il une luminosité typique de dessous de la quelle la fonction de luminosité s'effondre ?

  
\item[4 -] Est-ce qu'il nous manque une partie importante des galaxies formant des étoiles lorsque l'on se contente du processus de sélection LAE ou LBG ? Est-il possible d'obtenir une meilleure évaluation empirique des galaxies formant des étoiles en comprenant mieux la relation entre les populations de LAE et de LBG ? 
  
\item[5 -] Les LAE et les LBG sont-elles essentiellement la même population sans que nous soyons à même de nous rencontre à cause de biais observationnels ? \\
\end{itemize}


% --- Premier article
La majeure partie du travail effectué au cours de cette thèse a porté sur la détermination de la LAE LF derrière des amas de galaxies à effet de lentille gravitationnelle observés avec MUSE. Le but de cette étude était d'établir des contraintes sur la forme de l'extrémité faible de la LAE LF et de contraindre la contribution de cette population à la réionisation.

Profitant des grandes capacités de l'instrument MUSE et utilisant les amas de galaxies comme outil pour atteindre des LAE de faible luminosité, nous avons sélectionné à l'aveugle une population de 156 LAEs identifiés par spectroscopie derrière quatre amas, dans le domaine de redshift $ 2.9 < z < 6,7 $ et avec une luminosité \lya corrigée du grossissement  $39 \lesssim \log L \lesssim 43 $.
Étant donné la complexité de combiner les cubes de données spectroscopiques de MUSE avec les observations de lentille gravitationnelle, et tenant compte du fait que chaque source a besoin d'un traitement approprié pour rendre compte correctement son grossissement et de sa représentativité, le calcul de la LF a nécessité une mise en œuvre minutieuse, incluant quelques développements originaux. Pour ces besoins, une procédure spécifique a été développée, incluant les nouvelles méthodes suivantes. Tout d'abord, nous avons créé un calcul précis de \vmax pour les sources en arrière plan des amas qui est basé sur la création de masques 3D. Cette méthode nous permet de cartographier avec précision la détectabilité d'une source donnée dans des cubes spectroscopiques MUSE. Ces masques sont ensuite utilisés pour calculer le volume cosmologique dans le plan source de l'amas. Cette méthode pourrait être facilement adaptée pour être utilisée dans des travaux similaires en champs vide.
Deuxièmement, nous avons développé un calcul de la completude basé sur des simulations utilisant le profil réel des sources. Au lieu d'effectuer une approche paramétrique lourde basée sur des simulations d'injection et de re-detection de sources avec de bombreuses iterations. Cette méthode est plus rapide, plus flexible et prend mieux en compte les spécificités des sources individuelles, aussi bien dans la dimension spatiale que spectrale.

Après l'application de cette procédure à la population des LAE, la LAE LF a été construite pour différents bins de redshift en utilisant 152 des 156 LAE détectés. Quatre LAEs ont été supprimés parce que leur contribution n'était pas digne de confiance. En raison de la stratégie d'observation, cette étude fournit les contraintes à plus faible luminosité \lya obtenues à ce jour, par conséquent, une mesure plus précise de la densité de taux de formation stellaire associée à la population de LAEs. Nos résultats et conclusions peuvent être résumés comme suit :

\begin{itemize}
\item La population de LAE trouvée derrière les quatre amas a été divisée en quatre bins de redshift : 2,9 $ < z < 6,7 $, 2,9 $ < z < 4,0 $, 4,0 $ < z < 5,9 $ et 5,0 $ < z < 6,7 $. En raison de l'effet de lentille gravitaionnelles, le volume d'Univers sondé est considérablement réduit par rapport aux études en champs vide. Le volume moyen estimé de l'Univers examiné dans ces 4 bins de redshift est de $\sim 15\sim 15\,000$ Mpc$^3$, $\sim 5\,000$ Mpc$^3$, $\sim 4\,000$ Mpc$^3$, et $\sim 5\,000$ Mpc$^3$ respectivement.

\item La LAE LF a été calculée dans chacun de ces bins. De par la construction de cet échantillon, les LF dérivées sondent efficacement le régime de faible luminosité et la population observée fournit à elle seule des contraintes solides sur la forme de l'extrémité faible des LAE LF. Aucune évolution significative de la forme de la LF avec le redshift n'est trouvée en utilisant uniquement ces premiers points. Ces résultats doivent être pris avec prudence étant donné la nature complexe de l'analyse "lensing'', d'une part, et le petit volume effectif examiné par l'échantillon, d'autre part. Nos résultats plaident en faveur d'une possible sous-estimation systématique de la variance cosmique.


  \item Un ajustement de Schechter de la LAE LF a été effectué en combinant la LAE LF calculée dans cette analyse avec les données d'études antérieures pour contraindre l'extrémité brillante.  À la suite de cette étude, on a mesuré une pente prononcée pour l'extrémité faible, variant avec le redshift entre $\alpha = -1,58^{+0,11}_{-0,11}$ à $2,9 < z < 4$ et $\alpha = -1,87^{+0,12}_{-0,12}$ à 5 $ < z < 6,7 $.
  
\item Les valeurs de \sfrdlya ont été obtenues en fonction du redshift par l'intégration de nos LFs et comparées aux niveaux nécessaires pour ioniser l'Univers tels que déterminés dans \citet{Bouwens2015a}. Aucune hypothèse n'a été faite concernant la fraction d'échappement des photons Lyman-alpha et les valeurs de \sfrdlya calculées dans ce travail correspondent aux valeurs observées. En raison des bonnes contraintes obtenues sur la partie faible luminosité de nos LFs et d'une meilleure récupération du flux total, nous estimons que les résultats actuels sont plus fiables que ceux des études précédentes. Même si la population de LAEs contribue sans aucun doute à une fraction importante de la SFRD totale, il n'est pas certain que cette population suffise à elle seule à ioniser l'Univers à $z \sim 6$. Les résultats dépendent de la fraction d'échappement réelle des photons Lyman-alpha.

  
\item Les LAEs et les LBGs ont un niveau de contribution similaire à $z \sim 6$ au niveau total de SFRD de l'Univers. Selon l'intersection entre ces deux populations, l'union des populations LAE et LBG peut suffire à réioniser l'Univers à $z \sim 6$.

\end{itemize}
%conclusion sur l'utilisation de MUSE : sélection plus complète
Grâce à ce travail, nous avons montré que les capacités de l'instrument MUSE en font un outil idéal pour déterminer la LAE LF.
Étant une IFU, MUSE permet une étude aveugle des LAEs, homogène en redshift, avec une meilleure récupération du flux total par rapport au observations spectroscopiques classique avec à l'aide de fente. La fonction de sélection est également mieux comprise que par l'imagerie bande étroite.

Environ 20\% de l'échantillon actuel de nos LAEs n'a pas d'équivalent photométrique identifié, même dans les observations les plus profondes à ce jour (i.e le HFF). Il s'agit d'un point important à garder à l'esprit car c'est un premier élément de réponse concernant l'intersection entre les populations de LAE et de LBG. L'extension de la méthode présentée ici à d'autres champs d'amas devrait également permettre d'améliorer la détermination de la LAE LF et de rendre plus robustes les contraintes sur les sources de la réionisation. \\



Afin de mieux comprendre l'interrelation entre les populations de LAE et de LBG, nous avons entrepris une recherche simultanée de ces deux populations dans un volume donné. L'objectif de ce second travail était de voir s'il est possible de réaliser empiriquement une meilleure évaluation des galaxies à formation d'étoile en examinant l'intersection de ces deux populations. Le deuxième objectif était de caractériser davantage les biais intrinsèques à chacune de ces méthodes de sélection afin de déterminer si la ségrégation entre les LAE et les LBG n'est effectivement qu'un biais d'observation ou le résultat de différences intrinsèques.


L'étude menée sur A2744 a conduit aux résultats suivants :
\begin{itemize}

\item Pour les galaxies à faible continuum UV (\Muv $\ge 20.25$), la fraction des LAE parmi les SFGs augmente avec le redshift jusqu'à  $z \sim 6$, en accord avec les précédentes études \citep[e.g.][]{ArrabalHaro2018}.

\item La sélection des émetteurs \lya semble être plus efficace que la sélection LBG pour identifier les galaxies a faible continuum UV (\Muv > - 17) qui ne seraient pas détectées dans les observation profondes en champs vide. A cet égard, nos résultats sont en bon accord avec \citet{Maseda2018}.

\item Jusqu'à \Muv $\sim - 17$, les LBGs semblent fournir une bonne représentation des SFGs, en particulier lors du calcul du flux ionisant total dans les volumes explorés par les relevés actuels.

\item Il n'y a pas de preuve claire, basée sur nos résultats actuels, d'une différence intrinsèque entre les populations de LAEs et LBGs. Toutefois, une enquête plus approfondie sera nécessaire pour conclure sur ce point. En particulier, certaines tendances systématiques apparaissent entre ces deux populations, en ce sens que les galaxies les plus brillantes en UV semblent présenter un rapport \rf plus faible et qui augmentant  pour les galaxies a fort continuum UV. Ceci pourrait être une indication d'une distribution différente de poussière et d'étoiles en fonction de la luminosité UV. La mesure des pentes UV de ces galaxies pourrait fournir des informations supplémentaires à cet égard.

\end{itemize}





Afin de poursuivre et d'améliorer le travail effectué au cours de cette thèse, plusieurs pistes peuvent être suivies et sont résumées dans les paragraphes ci-dessous. La première et la plus simple est d'ajouter de nouveaux cubes en entrée pour le calcul de la LF. Des cubes supplémentaires augmenteraient le volume total de l'Univers exploré et, en moyenne, auraient tendance à diminuer l'impact de la variance cosmique sur la détermination de la pente de la LAE LF. De nouveaux cubes ajouteraient également des sources à fort grandissement ($\mu \gtrsim 30$) à l'échantillon. Et même si ces sources ont tendance à avoir un grossissement et une luminosité \lya mal caractérisés (voir la section \ref{subsec:image_plane}), il peut être possible d'obtenir des informations statistiquement pertinentes pour $\log L \lesssim 40.5$ (la limite de complétude estimée de la LF déterminée dans cette thèse) en accumulant suffisamment de ces données. Enfin, si nous parvenons à doubler le volume effectif de l'échantillon, il serait possible de calculer les LFs pour deux ensembles de données indépendants et similaires et d'obtenir ainsi une meilleure compréhension de l'impact de la variance cosmique sur nos résultats.

Plusieurs cubes supplémentaires sont déjà disponibles grâce à la collaboration  GTO ou grâce à d'autres collaborateurs externes et sont en cours de préparation pour le calcul de la LF. Les résultats obtenus de cette nouvelle analyse feront l'objet d'une prochaine publication en préparation.\\


Notre façon actuelle de comptabiliser les variances cosmiques pour chaque point de la LF pourrait être améliorée. En utilisant le calculateur en ligne de \citet{Trenti2008}, nous supposons une géométrie compacte pour les quatre champs et considérons la population dans chaque bin de luminosité individuelle des LFs comme indépendante. Aucune de ces deux hypothèses n'est correcte à proprement parler. La meilleure façon d'améliorer cet aspect du travail serait de tirer de nombreux champs de vue aléatoires dans les résultats des simulations cosmologiques, mais en pratique, cela reste difficile pour deux raisons. La première est que la surface effective de chacun de nos champs varie fortement en fonction du grossissement/luminosité considéré, et cet effet compliqué à reproduire dans des FoVs simulés. La seconde est que la faible luminosité de nos LAEs implique des masses de halo de matière sombre qui sont inférieures aux limites de résolution des modèles semi-analytiques actuels (T. Garel, communication privée), les rendant peu fiables pour nos besoins.\\


Une dernière possibilité concernant la LAE LF serait d'utiliser exactement la même méthode que celle présentée dans le chapitre \ref{chap:computing_lf} pour calculer la LAE LF en utilisant des observations en champs vides profonds faites par MUSE (principalement le HUDF) en les considérant comme un cas limite des lentilles gravitationnelles (c'est-à-dire $\mu = 1$ sur la FoV totale). Comparer les résultats ainsi obtenus a la LF calculé à l'aide d'une fonction de sélection paramétrique "classique" \citep[i.e.][]{Drake2017b,Herenz2019} permettrait de tester davantage notre méthode et éventuellement d'améliorer la façon dont nous tenons compte des objets fortement amplifiés en ajustant, par exemple, les valeurs d'amplification limite $\mu_{\rm lim}$. De plus, cela permettrait d'avoir une détermination unique de la LF sur une plus grande plage de luminosité et permettrait une optimisation plus pertinente d'une fonction de Schechter sans avoir besoin d'utiliser des ensembles de données externes pour en contraindre la partie brillante.\\


Enfin, pour compléter le travail effectué sur l'intersection entre la population LAE et LBG derrière le cluster A2744, la même analyse pourrait être effectuée en arrière-plan de l'amas A370 qui a une couverture MUSE profonde avec un excellent modèle de masse \citep{Lagattuta2019} et une photométrie profonde car faisant partie du programme HFF. L'augmentation du volume fortement a fort grandissement  ainsi sondé rendrait nos conclusions à faible luminosité \lya et/ou faible magnitude UV plus pertinentes. \\



Dans un proche avenir, deux nouveaux télescopes nous permettront de mieux comprendre l'ère de la réionisation et les premières galaxies : le ``James Webb Space Telescope (JWST) et le "Extremely Large Telescope" (ELT) qui sont prévus respectivement pour 2021 et 2025.


Le JWST est un observatoire spatial infrarouge et proche infrarouge avec un miroir d'un diamètre effectif de 6,5m. Il a été conçu pour succéder au télescope spatial Hubble et possède des capacités d'imagerie et de spectroscopie. La combinaison de la grande surface collectrice et de la couverture de longueur d'onde infrarouge le rend bien adapté à l'étude de l'Univers à grand redshift. On s'attend à détecter les premières galaxies dans une plage de redshift de 7 $ \lesssim z \lesssim 15 $ grâce soit à la détection de l'émission \lya par spectroscopie, soit de la détection d'un effet Gunn-Peterson par photométrie profonde.

L'ELT est un télescope terrestre qui sera équipé d'un miroir principal de 39 m et d'instruments couvrant le visible et le proche infrarouge. Cette surface collectrice  sans précédent offrira de nouvelles possibilités pour des relevés cosmologiques à grand redshift, de grande surface et extrêmement profonds. Il offre une synergie importante avec les observations du JWST en raison de sa capacité à effectuer des observations de suivi spectroscopiques à grande échelle de galaxies identifiées avec l'imagerie du JWST, et ce dans un domaine de redshift très similaire. De plus, sa grande surface collectrice offre une importante sensibilité pour l'analyse spectroscopique détaillée, en particulier pour les galaxies à continuum ou les galaxies à raies d'absorption.



Enfin, et pour terminer ce manuscrit, il convient de souligner que même à l'époque de l'ELT, l'utilisation d'amas de galaxies comme télescopes gravitationnels a un avenir prometteur. Il a été démontré que la technologie IFU efficace mise au point avec MUSE avait grandement amélioré la qualité de la reconstruction du modèle de masse et donc notre capacité à utiliser des amas pour voir plus faible et plus loin. Comme d'autres instruments de ce type sont en cours de développement (par exemple MOSAIC et HARMONI pour l'ELT), la méthodologie présentée dans cette thèse pourrait être considérée comme une approche pionnière pour exploiter la combinaison des données IFU et des amas afin de mieux comprendre le processus de formation des galaxies. Comme la profondeur des observations évolue comme la racine carrée du temps d'exposition, l'augmentation de la profondeur des observations par un facteur de deux nécessite quatre fois plus d'exposition. A cet égard, et parce que le temps de télescope est coûteux, les observations d'amas restent très compétitives et efficaces car la profondeur est instantanément augmentée sans coût supplémentaire autre que la complexité du traitement des données. Le programme HFF conçu pour repousser les limites d'observations du HST et dédié exclusivement à l'observation d'amas de galaxies massifs illustre parfaitement ce dernier point.

\end{otherlanguage}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
