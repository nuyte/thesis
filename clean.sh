#!/bin/zsh
rm *.aux
rm *.out
rm *.toc
rm *.lof
rm *.lot
rm *.log
rm *.cpt
rm *.bbl
rm *.blg
rm *.lox
rm *.mtc*
rm *.maf
rm *~
rm *_region*
rm -r auto

rm main*.cpt
rm main.run.xml
