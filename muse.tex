\chapter{The VLT/MUSE instrument}
\label{chap:muse}
\minitoc
\listoffixmechapter
\newpage

% -----------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------




\section{General overview and technical features}

MUSE (Multi Unit-Spectroscopic Explorer) is a large field of view Integral Field Unit (IFU) placed on UT4, one of the four VLTs at the ESO Paranal observatory, in Chile.  Most of the information provided in the following sections come from the ESO MUSE user manual or the ESO MUSE pipeline user manual \footnote{Both documents can be found online at \url{https://www.eso.org/sci/facilities/paranal/instruments/muse/doc.html}}.
The MUSE project was initiated by its current PI, R. Bacon in the early 2000s and the instrument itself was commissioned in 2014. It operates with the 8 m mirror of UT4 in the optical domain, between $\lambda = 4750 \si{\angstrom}$ and $\lambda =  9350 \si{\angstrom}$. Its spectral sampling is of 1.25\si{\angstrom} and its resolving power varies between $R = 2000 $ and $R = 4000$ at higher wavelength. MUSE has two observation modes: the Wide-Field Mode (WFM) and the Narrow-field Mode (NFM) which respectively have and angular Field of View (FoV) of $\ang{;1;} \times \ang{;1;}$ and of $\ang{;;7.5}\times \ang{;;7.5}$ and a spatial pixel size of  \si{\ang{;;0.2}} and \si{\ang{;;0.025}}. The spatial resolution of the NFM is typically of $0.05 - 0.08 \arcsec $ whereas that of the WFM is typically of $0.2 - 0.3 \arcsec$ (both figures quoted are without adaptive optics, see Fig. \ref{fig:muse_nfm} for a comparison between the spatial resolution of NFM and WFM).

Because of its higher spatial resolution, the NFM it well suited for more detailed studies of brighter objects. On the contrary, the WFM is more suited for surveys and for probing larger cosmological volumes. All observations used in this work were conducted using the WFM and unless specified otherwise, all examples and discussions are made assuming the use of the WFM . \\

\begin{figure}[!htb]
\centering
\includegraphics[width=\hsize]{Figures/muse_cube_exemple.pdf}
\caption{MUSE cube structure, for each spatial pixel, a full spectrum is available.}
\label{fig:cube_exemple}
\end{figure}


Being an IFU, the final data products are data cubes (hereafter just cubes; see Fig. \ref{fig:cube_exemple}): all spatial pixels of the FoV have a complete spectrum with the features mentioned in the previous paragraph. Another way to say this is that MUSE produces 3681 images every $1.25 \si{\angstrom}$ between $\lambda = 4750 \si{\angstrom}$ and $\lambda = 9350 \si{\angstrom}$. Hereafter, these monochromatic images will be referred to as layers or spectral layers. A single MUSE cube weights around 3Gb with a variance cube extension. Several MUSE cubes can be combined together to form a single mosaic cube (see e.g., the A2744 mosaic in Sect. \ref{subsec:muse_obs} which is more than 10 Gb of data).\\

As of mid 2017, MUSE WFM also uses Adaptive Optics (AO) to correct the front-wave distortions caused by turbulence in the high layers of the atmosphere. This system can be used for both the WFM and the NFM. The four sodium lasers (see Fig. \ref{fig:muse_lasers}) have a wavelength of $\lambda 5890 \si{\angstrom}$ which falls in the middle of the MUSE spectral range. Because the laser are powerful and create scattered light, all exposures taken with AO have the spectral range centred on the sodium emission masked. This mask is applied to the entire FoV and spans a spectral range of at least $ 150\si{\angstrom}$ depending on what exact filter is used. \\

\begin{figure}
\centering
\includegraphics[width=\hsize]{Figures/Muse_lasers.pdf}
\caption{UT4 telescope on which MUSE is mounted, with the four adaptive optics lasers in action.\\ \small{Credit ESO/F. Kamphues}}
\label{fig:muse_lasers}
\end{figure}

The reduction of MUSE cubes is an entire subject in itself and is not addressed in details in this work. More information on the MUSE ESO reduction pipeline can be found in \citet{Weilbacher2012,Weilbacher2014} or on the ESO website\footnote{\url{https://www.eso.org/sci/software/pipelines/muse/}}. The basic feature of this reduction process are bias subtraction, flat-fielding, wavelength and flux calibration, basic sky subtraction and astrometry. All exposures are reduced individually before being combined into a final data cube.








 
\section{Main science goals of MUSE}

The combined use of a very large light collecting area, adaptive optics with the possibility to use WFM of NFM makes MUSE a very versatile instrument. It has been used in a lot of different science fields since its commissioning, from planetary and small body science to stellar population in nearby galaxies  and high redshift studies. This section aims at providing a wide but non exhaustive overview of the motivations at the origin of the MUSE project as well as a glimpse of the various work that have been conducted taking advantage of the various abilities of the instrument. The different thematic fields are listed below by order of priority as exposed in the original science goals of the instrument\footnote{The original science goals as determined at the beginning of the MUSE project in 2004 are can be found online at \url{http://muse.univ-lyon1.fr/spip.php?rubrique2}}.\\


% -- Formation and evolution of galaxies
% -- high redshift stuffs
The study of evolution and formation  of galaxies and of the early Universe was the main driver for the development of MUSE. Most of the design of the instrument is therefore optimized for this specific kind of science. In this field, as often a detailed spatial resolution is not an absolute necessity, the WFM is most often used to probe larger volumes of Universe.

% lya selection
Distant galaxies are difficult to detect as they appear faint. An efficient and reliable  way to select high redshift star forming  galaxies is through their \lya emission line (see Sect. \ref{subsec:laes} for details on the \lya line). One of the main focus of MUSE is the detection of this line between $2.9 < z < 6.7$ to reliably select faint and high redshift galaxies.  The \lya selection is not an end on itself, but gives access to a new faint population of galaxies that was mostly out of reach before the advent of MUSE. The identification of these low luminosity low mass galaxies is for example, a crucial point to set constraints on the hierarchical scenario of galaxy formation \citep[see e.g.,][which studies the galaxy merger fraction]{Ventou2017}, to study the sources of reionization(see e.g., \citet{Drake2017b,Hashimoto2017,Japelj2017,Caruana2018} and \citet{deLaVieuville2019}) or the star formation rate history. In addition to these, the peculiar line profile of the \lya line  can be used to look for possible inflow or outflow of neutral hydrogen in high redshift galaxies (see e.g., \citet{Verhamme2006,Swinbank2015,Guaita2017} or Sect. \ref{subsec:laes})

% cosmic structures.

According to the $\Lambda$-CDM theory of cosmology,  one of the first structures to form in the Universe is the cosmic web (see Sect. \ref{subsec:structure_formation}). This cosmic web structure can be indirectly observed and traced with a 3D mapping of galaxies provided that we have their redshift. Such work has been done with the galaxies of the SDSS survey in \citet{Sousbie2011} and \citet{Tempel2014}. Since MUSE allows the detection of very faint LAEs (see Sect. \ref{subsec:elines_detection}) compared to previous facilities, it can provides new observational constraints on this structure as a by-product of any observation. Direct observation of these filamentary structures through  diffuse \lya emission of the gas remain very challenging due to the extremely low surface brightness  of such an emission. It has been attempted in \citep[see e.g.,][]{Gallego2018,Cantalupo2019}, and while the diffuse \lya emission can be seen in the close neighborhood of galaxies, nothing really conclusive can be seen at intergalactic scales.\\


% -- ~? nearby galaxies
At lower redshift (mostly z < 1) one of MUSE goals was to produced detailed analysis of the galactic environment and its mechanisms including studies of the inter-galactic medium, galactic winds, feedback and galaxy kinematics. Taking advantage of the higher spatial resolution on these closer galaxies, MUSE allows a more in depth study and understanding of the mechanisms playing a possible role in the evolution of galaxies. The conclusion of such studies can also be transposed to the study of higher redshift galaxies for which such work is not possible. This can be done using for example, spatially resolved spectroscopy in QSO line of sights to probe the possible presence of circumgalactic gas or galactic winds \citep[see e.g.,][]{Schroetter2016,Bouche2016,Zabl2019} or  studying gas or stellar kinematic  by mapping the Doppler effect of emission lines across spatially resolved galaxies \citep[see e.g.,][]{Contini2016,Guerou2016,Guerou2017}.
At these lower redshift, MUSE can also be used to study punctual and rarer events such as the on-going merger in the  Antennae galaxy \citep[see .e.g.,][]{Weilbacher2018,Monreal-ibero2018} or specific studies of the strangest galaxies found in the SDSS survey (see \citet{Baron2017} and Monreal-Ibero in prep.)\\


% -- star and star population : GC and crowded stellar fields

The IFU capabilities of the instruments comes in handy when studying crowded fields as it allows to capture a very large number of spectra within a single observation.
Conducting massive spectroscopy in globular clusters can help understanding the evolution of star population in such environments and allows comprehensive studies of the velocity field and dispersion \citep[see e.g.,][]{Husser2016,Kamann2016,Kamann2018a,Kamann2018b}. This observational work contributes (among many other things) to the investigation of the formation mechanisms of globular clusters and the search for possible black-holes at their center.

In dense stellar fields MUSE observations allows to produce several thousands of stellar spectra \citep{Roth2018}  which in itself represents a legacy value for the community and can be used for many different applications including the study of the chemical evolution of galaxies, the study of star formation history and the possibility to complete previous stellar libraries with un-biased and complete blind spectroscopic  selection.

For such observations, and especially when looking at the core of globular clusters, the use of the NFM with AO can give the necessary spatial resolution needed to dissociate individual stars form their neighbours. Figure \ref{fig:muse_nfm} provides an illustration of the resolution reached with the NFM with AO in the center of a globular cluster, and a comparison of the resolution reached with HST on Neptune.\\



\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\textwidth]{Figures/MUSE_NFM.pdf}
\caption{Top: comparison of the spatial resolution reached with MUSE WFM without AO
 and with MUSE NFM with AO. Bottom: Comparison of the spatial resolution reached with MUSE NFM with AO, with HST.\\ \small{Credits: ESO/S. Kamann (LJMU) (top), ESO/P. Weilbacher (AIP)/NASA, ESA, and M.H. Wong and J. Tollefson (UC Berkeley)(bottom)}}
\label{fig:muse_nfm}
\end{figure}

% -- solar system
With the use of the NFM, MUSE was also intended to be used in the study of small bodies and planets in the solar system. While some observations were made during various commissioning runs (See e.g,, Neptune in Fig. \ref{fig:muse_nfm}), almost no literature can be found on the subject.\\




In this Section we have shown that due to its very wide abilities, MUSE can be (and has been) used in virtually any field of astrophysics. However, up to now, most science products and publications come from the MUSE deep and very deep surveys (with a special focus on the LAEs), since the telescope time has been divided according to the priority of the different science cases exposed in this section. In the following sections, we focus on the use of MUSE to study the population of SFGs at $2.9 < z < 6.7$ based on their \lya emission. In Sect. \ref{subsec:elines_detection} we explain why MUSE is so efficient to detect emission line objects compared to other instrumental techniques and we show that its capabilities makes it an ideal instrument to study the galaxy LF in Sect. \ref{subsec:blind_spectroscopic_selection}.








\section{MUSE to study the galaxy population}
\label{sec:muse_to_study_galaxies}
\subsection{Detection of faint emission line objects}
\label{subsec:elines_detection}

The main motivation for the conception of MUSE was to study faint and distant galaxies, with a focus on the detection of hydrogen emission lines associated with the presence of young star populations and the formation of stars. It has been shown on several occasions that MUSE is indeed most efficient to detect emission lines objects
\citep[see e.g.,][where a lot of emission line emitters have been detected without any identified counterpart on very deep HST images]{Bacon2015,Bacon2017,Herenz2017,Mahler2018,Maseda2018}. The efficiency of MUSE resides in the optimization of the signal to noise around emission lines thanks to its IFU capabilities.\\


% how  to usually detect e-lines
% -- NB imaging
One of the classical approach to select emission line emitters is the use of Narrow-Band imaging. A preliminary selection is done by looking for a flux excess in certain band that would be compatible with the presence of an emission line at a certain redshift. The use of NB makes this technique only sensitive to a flux excess in a reduced redshift range, depending on the spectral width of the NBs. This exercise can be repeated to select emission-line emitters centered on different narrow redshift ranges. The use of broader filters obviously enlarges the redshift range of the selection, but broader filters are also less sensitive to a potential flux excess caused by an emission line which is by nature, quite narrow. As a second step, a spectroscopic follow up of the sources that passed the first selection can be done. This technique, or some variations of it have often been used to select a LAE population in order to study its LF \citep[see e.g.,][and many more]{Rhoads2000,Westra2006,Dawson2007,Hu2010,Ouchi2010,Matthee2015,Santos2016,Sobral2018} but is not only limited to the selection of the \lya line.

% -- Blind slit spec search
As an alternative to the NB imaging techniques, a blind spectroscopic search for line emitters has been developed by looking for serendipitous detections in slits of multi-slits instruments \citep[see e.g.,][]{Martin2004,Rauch2008,Cassata2011,Cassata2015,Dressler2015}. In this method, the regions of slits free from the presence of target sources are used for "blind'' detection of line emitters. The term "blind" is used as contrary to the NB imaging with spectroscopic follow-ups, no photometric priors  are used to spectroscopically identified line emitters. The main advantage of such method is that it is less biased in its selection than NB surveys and allows for the detection of fainter line emitters.  However, and as detailed in next section, it suffers from a systematic loss of flux for extended sources.\\


% Comparison of the efficiency of MUSE and NB / BB imaging  / blind slit spec
% selection
By construction, narrow-band imaging techniques are  sensitive to an integrated flux on a given spectral range, which make them very efficient to measure the continuum of faint galaxies, as the spectral integration increases the signal-to-noise of the continuum. On the contrary, and as mentioned in the previous paragraph, the excess flux caused by the presence of an emission line, can easily be lost in the continuum integration. MUSE is the opposite of NB imaging: it is efficient to detect emission lines, but not so to measure and detect faint continua. Being an IFU, the signal of a galaxy continuum is divided on many spectral pixels which results in an increase of both the photon noise and the relative contribution of the read-out noise of the CCDs. The signal of the continuum is therefore divided and the noise increased, making the detection and measurement of faint continua difficult with MUSE. On the contrary, the signal of emission lines is spectrally compact and brighter than the continuum. As the emission lines are not diluted in a spectral integration, and because of the large light collecting area, they easily rise above the noise in MUSE cubes. Blind spectroscopic search for line emitters are quite similar to IFUs in terms of detection abilities but do not provide complete spatial information nor do they allow to precisely measure the flux.\\




\subsection{Advantages of a blind spectroscopic selection for the LAE LF}
\label{subsec:blind_spectroscopic_selection}

While the NB imaging and blind search with slit spectroscopy techniques have been proven to work quite well and have been widely used, they both have certain flaws that can be avoided when using an IFU. In this section we discuss the advantages of the MUSE selection and measurements with respect to other instrumental techniques to show that MUSE is the ideal instrument to study the faint end of the LAE LF and the sources responsible for reionization.\\


% -- Flux loss
The first and maybe the most important default of NB surveys is the potential loss of flux when the emission line is cut by the border of the NB filter or when spatially extended galaxies do not fit entirely in the slit used for the spectroscopic follow up. A systematic underestimation of the flux of LAE results in a shift of the entire LF towards lower luminosity. Such systematic loss is studied in \citet{Cassata2011} for LAEs. This study shows that in average 15\% of the flux is lost, with variations ranging from 10\% to 30\% depending on the seeing conditions. In addition to the luminosity shift, there are also some systematic effects on the shape of the LF: lower luminosity  or higher redshift galaxies are less affected by the loss of flux, as in average they appear smaller on the sky. Using MUSE, we systematically recover a greater fraction of the total flux as we recover the entire spectral and spatial profile of any emission line emitter. For LAEs, this aspect is especially important as it has been shown in \citet{Wisotzki2016},\citet{Leclercq2017} and \citet{Wisotzki2018} that LAEs tend to have extended halos.

% -- Completeness of the selection
The second point is the completeness of the selection. Using NB imaging, the LAEs are selected by detecting an excess of flux with respect to the continuum detection which automatically remove the objects with non detectable continuum that are nevertheless LAEs. In addition, this technique is only reliable to select bright line emitters, and fails to identify the faintest objects. A significant fraction of the LAE population is therefore missed using NB surveys only. Using blind spectroscopic search with slit spectroscopy provides more complete samples, but as mentioned in the previous paragraph, this type of selection is affected by systematic loss of flux.
Using MUSE, we have a truly blind spectroscopic selection of all line emitters in a given volume and systematic effects are expected to be small. This allows to properly assess the completeness of the selection, measure the volume probed during the observations and compute accurate flux estimates for all LAEs. All these three elements are key steps when computing a LF. As a side note, this complete blind spectroscopic selection of sources is also a huge advantage for the mass modelling of galaxy clusters since it allows the discovery and confirmation of new multiple images systems that are essential for these precise needs \citep[see e.g.,][and many more]{Richard2015,Bina2016,Caminha2016,Mahler2018,Lagattuta2017,Lagattuta2019}.


% -- Continuous redshift range
The third and last point is the continuous redshift selection of LAEs between $2.9 < z < 6.7$ with MUSE. This wide redshift range is interesting to study the sources of reionization as it just overlaps with the end of the reionization era at $z \sim 6$, and allows to study a possible evolution of the LAE LF over a wide range of redshift.\\

To sum up this section, MUSE allows a complete and as unbiased as possible selection of a LAEs in a single observation. It probes a wide range of luminosity and redshift while offering the possibility of systematically recovering a larger fraction of the LAE flux. The large collecting area combined with the IFU structure makes it possible to detect faint LAEs whose continuum would not have been detected otherwise with NB imaging. All of these points combined makes MUSE the ideal instrument to work with to probe the faint end of the LAE LF.\\


To further extend the sensitivity to the low luminosity LAEs, strong lensing clusters are used in this work. Because of the volume contraction induced by lensing effects  (see Chpt. \ref{chap:lensing_sample}) the volume probed in such FoV drastically shrinks which makes it more difficult to get a statistically relevant sample of LAEs. Even without the presence of galaxy clusters in the line of sight, the volume probed by MUSE cannot compete with the large volume probed with large NB surveys. For example the MUSE HUDF the total covolume between $2.9 < z  < 6.7$ probed on the $3\times 3$ MUSE mosaic is around ~ 9.4$\times 10^4$\si{Mpc}$^3$ which is far more than can be reached in the high magnification regions of lensing fields: the total covolume probed in this work with 4 cubes  is around 1.6$\times 10^4$ \si{\Mpc}$^3$.  For comparison, in \citet{Sobral2018} the total covolume probed within the 2 square degree of the COSMOS field using a collection of broad and narrow bands to look for LAEs is about 6.2$\times 10^7$\si{\Mpc}$^3$. This shows that even though MUSE is a nice instrument to look in depth in a given volume, large NB surveys are still much needed to probe large cosmological volumes. And more specifically in the study of the LF, faint galaxies being much more numerous than bright ones, such large volumes are required to study the bright end of the LF. 






\section{Noise structure in MUSE cubes}
\label{sec:noise_structure}

In this section we pay close attention to the noise in cubes to better understand its structure because it can affect the source detection. What we define as noise in this work is the range of random variation from pixel to pixel with respect to a "true value". A higher level of noise means a higher dispersion of pixel value with respect to this true value, and a lower noise level means a lower dispersion. Because any noise measurement relies on the measurement of pixel variations, it is necessarily averaged on a local area (i.e., we cannot measure any variations on a single pixel). In astronomical data, the main sources of noise can be grouped under three broad categories: the photon noise, the noise caused by the instrument itself and the read-out noise caused by the electronics of the instrument.


% Poissonian noise of sources
Following the definition above, the presence of a source locally increases the noise. Indeed, assuming that the photon count of a source follows a Poissonian distribution (as it is often assumed) the photon count is proportional to $F$, the flux of the source and the variations (or uncertainty) are proportional to $\sqrt{F}$. This simply shows that the absolute photon noise increases with an increasing flux, even though the signal-to-noise ratio of the source increases as $\frac{F}{\sqrt{F}} = \sqrt{F}$. 

In addition to this phenomenon, there are at least three constant noise components in MUSE cubes that are always present regardless of the observation being carried out. Some of them are intrinsic to the instrument and caused by its design, and some are external to the instrument itself. All these different components play an important role in the understanding of the volume computation described in Chpt. \ref{chap:3d_masks}. For this reason they are detailed in Sect. \ref{subsec:muse_slicers}, Sect. \ref{subsec:sky_lines} and Sect. \ref{subsec:sensitivity}, and some illustrations are provided.


\subsection{Structure of the instrument}
\label{subsec:muse_slicers}


When the light enters the instrument, it is split in 24 by a field separator that  redirects the light towards the 24 spectroscopic units of MUSE. Before reaching each of these units, each beams is split again into 48 smaller beams by slicers that rearrange each beams on the 24 CCDs, in a complex combination of spatial and spectral positions (see Fig. \ref{fig:muse_slicers}). During the reduction process, the data is collected across the 24 CCDs and is combined into a single data cube. Even after the end of the reduction process, the architecture and organisation of these field separators and slicers remain visible on some of the spectral slices of the cubes as illustrated in the right panel of Fig. \ref{fig:muse_slicers}. These patterns are especially visible in cubes with long exposure time, since the relative contribution of photon noise decreases  with exposure time.


There are several possible explanations for this effect, starting from different sensitivities on different part of a same CCD or different sensitivity from one CCD to another, or from a slight misalignment when combining all spectra into a cube. To minimize the slicer patterns on the final cubes, a \si{\ang{90}} rotation is often used between each individual exposure of a same field. In this situation, the patterns appear as squares as seen in right panel of Fig. \ref{fig:muse_slicers}.


These differences are accounted for during the reduction and added into the variance extension of each cube. They nonetheless remain visible in the main data cube and affect the detection of sources and therefore play an important role when computing the effective volume as described in Sect. \ref{sec:final_method_3d_masks}. The effects of the structure described in this section are not visible on all slices of a cube. However, the patterns and their spatial layout are almost identical on all slices where they are visible.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{Figures/slicer_structure.pdf}
\caption{Left panel: the 24 field separators and the 48 slicers on the MUSE FoV and layout of the 48 spectral strip on one of the 24 CCDs of the instrument. Each strip is about 3600 spectral pixels long and has in average a width of $\sim 76.5$ spatial pixels according to the MUSE pipeline User Manual.}
\label{fig:muse_slicers}
\end{figure}



\subsection{Sky emission lines}
\label{subsec:sky_lines}

Because MUSE is an instrument based on Earth working in the optical domain, it is subjected to bright sky emission consisting of a slowly varying continuum and bright emission lines. The wavelengths of each possible sky lines are well known and affect certain spectral layers of the cubes with various intensities. The sky lines can be further removed after reduction, with the use of ZAP (Zurich Atmosphere Purge, \citet{Soto2016}). This software uses a principal component analysis to characterize the sky lines residuals and further subtracts them. If most of the line flux is removed, the increased Poissonian variance caused by the bright sky emission remains on the affected layers. For each of those spectral layer, this results in and increased but constant level of noise across its entire spatial extent. Some of the sky lines are so bright that they leave MUSE almost blind in certain layers, leaving only the brightest sources to be possibly detected there. Sky lines are not homogeneously distributed (see Fig. \ref{fig:variance_levels} for an example) and most of them fall in the redder part of MUSE wavelength coverage making it harder to detect high redshift emission line emitters. 


\subsection{Sensitivity of the instrument and final combination}
\label{subsec:sensitivity}


\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]{Figures/A2390_rms_level_with_wav.pdf}
\caption{Relative variation of the average background noise computed from every layer of the A2390 cube. More details on this specific computation are provided in Sect. \ref{subsec:definitions_noise_levels}.}
\label{fig:variance_levels}
\end{figure}

The last component, is the sensitivity of the instrument itself. During commissioning, it has been established through the computation of the throughput of the instrument that MUSE is most efficient at around $7000 \si{\angstrom}$. This increased efficiency automatically translates into a reduced noise as the photon count is more precise. When measuring the relative averaged background noise for every wavelength step of a MUSE cube as it is done in Fig. \ref{fig:variance_levels}, we in fact measure the added noise contribution of the instrument sensitivity, the continuum sky emission and the sky emission lines. From this figure, we indeed see that the relative noise reaches a minimum around 7\,000\si{\angstrom}. The slow continuum variation are mostly guided by the sensitivity of the instrument and the sky continuum emission. The spikes, mostly seen in the redder part of the MUSE wavelength coverage, at $\sim 7\,200\si{\angstrom}$ and above, are caused by the sky-emission lines. In addition to these relative and average variation of noise along the spectral axis of a MUSE cubes, for each layer, there is a spatial variation of noise governed by the layout of the slicers and field separators as shown in right panel of Fig. \ref{fig:muse_slicers} (and possibly the rotation of the FoV during the data acquisition).









\section{Cataloging sources in a MUSE FoV}
\label{sec:cross_match}

Because of the 3D structure of the data, visualisation becomes quite challenging, and  manual inspection of large numbers of sources or large sections of cubes quickly becomes very limited. For that reason specific tools have been developed within the MUSE collaboration to facilitate common, repetitive and or complex tasks.

A specific python module, MPDAF \citep{Piqueras2017}, was developed  to make easier the analysis of MUSE cubes. It allows among other things, to handle MUSE cubes, images and spectra with dedicated classes and functions. For source detection,  several alternatives have been developed including \textsc{CubExtractor} \citep{Cantalupo2019}, based on an connecting-labelling component algorithm, \textsc{LSDcat} based on a three-dimensional matched filter approach \citep{Herenz2017_lsdcat}, \textsc{Origin} \mbox{\citep{Bacon2017}} based on 3D filter matching and finally \muselet which is part of the MPDAF package and is based on \sex \citep{Bertin1996}. In this work, only \muselet is used, and because this software plays an important role in the method developed in this thesis, a more detailed description of it is provided in Sect. \ref{sec:source_detection}.\\


When cataloging a certain area of the sky, the goal is to be as complete as possible. However, as explained in Sect. \ref{subsec:elines_detection}, MUSE is not the most efficient to detect continuum sources and is therefore likely to miss some of them. To have an as complete as possible list of sources in a given area, the best strategy is to cross match the MUSE detections with HST detections. This process yields three categories of sources:

\begin{itemize}
\item Line emitters only (MUSE detection only)
\item Line emitters with detected continuum (MUSE + HST detection) 
\item Continuum only (HST detection only)
\end{itemize}

In addition to the complete assessment of sources in a volume, the cross matching is necessary to get photometric information and more precise spatial information for the MUSE detections that are successfully matched to an HST detection. The higher spatial resolution of HST is also useful to improve the source segmentation, and therefore to optimize the spectrum extraction in MUSE cubes. When working in lensing clusters, it can also help for the identification of multiple images. Even though some part of the cross match can be automated trough a simple coordinate matching procedure, the identification of an HST counterpart sometimes remains tricky as the spatial resolution of MUSE and HST are so different. Figure \ref{fig:muse_hst_comparison} provides a comparison of the spatial resolution seen by MUSE and HST.
To avoid as much as possible doing any mis-match, it is therefore needed that all sources of both catalogs are manually reviewed, which can be tedious work for a single person depending on the depth of the observations and the area surveyed.
For that reason, some collaborative tools have been developed within the MUSE collaboration to make it easier to go quickly through a large number of sources and to share the work load between different groups of people. \\

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{Figures/MUSE_HST_comparison.pdf}
\caption{Comparison of the spatial resolution of MUSE (Left) and HST(right) on a region of the A2744 lensing cluster. The MUSE image is a white light  image (i.e., the sum of all spectral layers into a single image). The comparison of the two images is obviously a bit limited since by nature MUSE data are cubes, and HST data are single images.}
\label{fig:muse_hst_comparison}
\end{figure}

 

To give an example of the numbers at stake, in the MUSE HUDF and UDF-10 \citep{Bacon2017} mosaic, more than 1\,500 candidate emission lines objects have been identified. Each of these candidates were manually inspected to assess the robustness of the detection, validate or not the corresponding HST detection (if any was found), assess the quality of the spectrum extraction and whether the sources is being contaminated by nearby neighbours and measure the redshift. The entire process is very tedious and is described in \citet{Inami2017}. At the end of the process, a secure redshift was measured for 1\,338 galaxies.\\

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
