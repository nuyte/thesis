\begin{center}
\huge
\textbf{Abstract}
\end{center}
\vspace{0.5cm}
\normalsize



  Reionization is the last change of state of the Universe which made its entire  hydrogen content transition from a neutral to a completely ionized state. This rapid transition and heating of the gas content had major consequences on the formation and evolution of structures which makes of reionization a key element to understand the present day Universe.


  In our current understanding, reionization was mostly done by $z \sim 6 $ and the sources responsible for this transition are likely faint, low mass and star-forming galaxies (SFGs). One way to study this population is to determine the Luminosity Function (LF) of galaxies selected from their Lyman-alpha emission and assess their ionizing flux density. However, most recent studies and their conclusions are in general limited by the lowest flux that can be reached with the current observational facilities.

One of the major goals of the work presented in this manuscript is the study of the Lyman-alpha emitters (LAE) LF using deep observations of strong lensing clusters with the VLT/MUSE instrument. MUSE is a large field of view Integral Field Unit (IFU) ideal to work on the galaxy LF since it allows a complete and blind selection of LAEs without any photometric preselection. In addition, MUSE provides a continuous redshift range of $2.9 < z < 6.7$ for LAEs that just overlaps with the end of the reionization era, making MUSE well suited to provide constraints on the contribution of LAEs to reionization.


The combined usage of large IFU data cubes and lensing fields makes this analysis computationally challenging. To get around this difficulty, we have developed new methods to account for the contribution of each individual LAE, including the effective-volume and completeness determinations. The volume computation is based on the simulation of the detection process of individual LAEs in the source plane reconstruction of MUSE cubes to account for both the lensing effects and the individual spatial and spectral profiles of LAEs. Throughout this work special care was given to faint and highly magnified LAEs since they are the key to access the very faint end of the LAE LF.
To the cost of a significant increase in complexity and a lower volume of Universe explored, both due to the lensing effect, we build the LAE LF using a Monte-Carlo process to account for all possible sources of uncertainties, for a population of 152 LAEs with  $39 \lesssim \log L_{\rm Ly_{\alpha}} \text{[erg s}^{-1} \text{]}\lesssim 43$ selected across four MUSE fields of view.


The LFs resulting form this analysis set an unprecedented level of constraint on the shape of the faint end and reached down to $\log L = 40.5$ . We conclude, making no assumption on the escape fraction of \lya emission, that the LAE population has a similar level of contribution to the total ionising emissivity as the UV-selected galaxies (LBGs) at $z \sim 6$. Depending on the relative intersection of these two populations, the SFGs could produce enough ionising flux to fully ionise the Universe by $z \sim 6$. 

In the continuity of this work on the LAE LF, we investigate the effect of the selection method on the conclusions mentioned above. A better characterisation of the bias intrinsic to the LAE and LBG selection processes is needed to reach a more robust assessment of the complete population of high redshift SFGs. To this end, we implemented a blind and systematic search of both LAEs and LBGs behind lensing field A2744 using the deep MUSE observations in combination with public data of the Hubble Frontier Fields (HFF) program. The results have shown that the observed proportion of LAEs increases significantly among UV-faint galaxies and at increasing redshift.


\newpage
\begin{otherlanguage}{french}
\begin{center}
\huge
\textbf{Resumé}
\end{center}
\vspace{0.5cm}
\normalsize



La réionisation est le dernier changement d'état de l'Univers qui a fait passer la totalité du contenu en hydrogène d'un état neutre à un état complètement ionisé. Cette transition rapide et le réchauffement de la phase gazeuse ont eu des conséquences majeures sur la formation et l'évolution des structures qui font de la réionisation un élément clé pour comprendre l'Univers actuel.


  Dans notre compréhension actuelle, la réionisation était principalement finie autour $z \sim 6 $ et les sources responsables de cette transition sont probablement les galaxies peu lumineuses, de faible masse et à formation d'étoiles (SFG). Une façon d'étudier cette population consiste à déterminer la fonction de luminosité (LF) des galaxies choisies grâce à leur émission Lyman-alpha et à évaluer leur densité de flux ionisant. Cependant, les études les plus récentes et leurs conclusions sont en général limitées par la limite en flux qui peut etre atteinte par les instruments actuels.

L'un des principaux objectifs du travail présenté dans ce manuscrit est l'étude des émetteurs Lyman-alpha (LAE) LF à l'aide d'observations profondes d'amas de galaxies à effet de lentilles fortes avec l'instrument VLT/MUSE. MUSE est un "Integral Field Unit'' (IFU) à grand champ de vision idéal pour travailler sur la LAE LF puisqu'il permet une sélection complète et aveugle de LAEs sans aucune présélection photométrique. De plus, MUSE offre une plage de redshift continu de $2.9 < z < 6.7 $ pour les LAE qui coïncide avec la fin de la réionisation, ce qui rend MUSE très pertinent pour fournir des contraintes sur la contribution des LAEs à la réionisation.


L'utilisation combinée de grands cubes de données IFU et de champs lentillés rend cette analyse difficile sur le plan informatique. Pour contourner cette difficulté, nous avons mis au point de nouvelles méthodes pour tenir compte de la contribution de chaque LAE, y compris la détermination du volume effectif et de la complétude. Le calcul du volume est basé sur la simulation du processus de détection des LAEs individuelles dans la reconstruction du plan source des cubes MUSE, pour tenir compte à la fois des effets de lentille et des profils spatiaux et spectraux individuels des LAEs. Tout au long de ce travail, un soin particulier a été apporté aux LAEs de faible luminosité et fortement lentillés car ils sont la clé d'accès à l'extrémité à faible luminosité de la LAE LF.
Au prix d'une augmentation significative de la complexité et d'une diminution du volume de l'Univers exploré, tous deux dus à l'effet de lentille, nous construisons la LAE LF en utilisant un processus de Monte-Carlo pour tenir compte de toutes les sources possibles d'incertitudes, pour une population de 152 LAEs avec $39 \lesssim \log L_{\rm Ly_{\alpha}} \text{[erg s}^{-1} \text{]}\lesssim 43$ sélectionnés parmi quatre champs de vision MUSE.


Les résultats de cette analyse ont établi un niveau de contrainte sans précédent sur l'allure à faible luminosité de la LF jusqu'à $\log L = 40.5$ . Nous concluons, sans faire d'hypothèse sur la fraction d'échappement de l'émission \lya, que la population de LAE a un niveau de contribution à l'émissivité ionisante totale similaire à celui des galaxies sélectionnées par imagerie UV (LBG) à $z \sim 6$. En fonction de l'intersection relative de ces deux populations, les SFG pourraient produire suffisamment de flux ionisants pour ioniser complètement l'Univers à $z \sim 6$. 

Dans la continuité de ce travail sur la LAE LF, nous étudions l'effet de la méthode de sélection sur les conclusions mentionnées ci-dessus. Une meilleure caractérisation du biais intrinsèque aux processus de sélection LAE et LBG est nécessaire pour parvenir à une évaluation plus robuste de l'ensemble de la population des SFGs à haut redshift. À cette fin, nous avons effectué une recherche aveugle et systématique des LAEs et des LBGs derrière l'amas de galaxies A2744 en utilisant les observations MUSE  en combinaison avec les données publiques du programme Hubble Frontier Fields (HFF). Les résultats ont montré que la proportion observée de LAEs augmente de manière significative parmi les galaxies à faible continu UV et et parmi les galaxies à haut redshift.






\end{otherlanguage}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
