\begin{otherlanguage}{french}

  \chapter*{Avant propos}
  \addstarredchapter{Avant propos}


Un moteur important du développement de l'astronomie et de la cosmologie est la volonté de voir toujours plus loin et surtout plus loin dans le temps, afin de comprendre le tout début de l'Univers.
Dans cette chasse de longue haleine aux objets éloignés et à grand redshift, une grande partie des efforts d'observation ont été orientés vers l'étude de la réionisation et des sources responsables de cette réionisation cosmique.


La réionisation est le dernier changement majeur de phase de l'Univers. Après la recombinaison, le contenu en hydrogène de l'Univers était en grande partie neutre et, à mesure que l'Univers se développait, ce gaz neutre s'est refroidi et les premières étoiles et galaxies se sont formées. Les lumières ionisantes émises durant cette période ont fait passer l'Univers d'un état essentiellement froid et neutre à un état essentiellement chaud et ionisé. Ce réchauffement de l'Univers a eu un impact significatif sur la formation des structures et à toutes les échelles spatiales. La réionisation est donc un élément clé pour comprendre l'évolution de l'Univers et les observations actuelles.


Jusqu'à présent, on sait peu de choses sur les sources responsables de la réionisation. Il a été établi au fil des ans que les quasars, les sursaut gamma et les binaires X n'étaient pas assez nombreux pour entraîner cette transition, laissant les galaxies "normales" à formant des étoiles (SFG) comme les plus probables candidats. Cependant, il n'y a pas de preuve d'observation directe de ce fait. Des études antérieures sur la fonction de luminosité (LF) des galaxies, qui est la distribution de la densité numérique des galaxies par intervalle de magnitude/luminosité bin, ont révélé que pour que les SFG dominent la réionisation, la contribution des galaxies peu brillantes et faible masse doit être décisive. Mais par essence, il est difficile d'observer des SFGs de faible masse et de faible intensité intrinsèque, surtout à haut redshift. Pour parvenir à des conclusions plus définitives, des observations plus profondes (et plus étendues) sont nécessaires, et c'est pourquoi les progrès réalisés dans la compréhension de la réionisation sont étroitement liés au développement de nouveaux télescopes et instruments, sans cesse plus efficaces. \\


Un autre sujet d'intérêt dans cette recherche des SFGs est la question de la méthode de sélection. Deux méthodes sont couramment utilisées pour sélectionner les SFG à grand redshift : la sélection dite "drop out'' basée sur des observations UV multi bandes, et la sélection Lyman-alpha (\lya) basée sur l'identification spectroscopique de la raie d'émission \lya. Étant donné que chacune de ces deux méthodes a ses propres biais d'observation, il demeure difficile d'avoir une évaluation complète des SFGs sans une meilleure compréhension et caractérisation de l'intersection des populations sélectionnées par ces deux méthodes. Jusqu'à présent, les populations sélectionnées à partir  d'image UV (LBG) et les émetteurs de Lyman-alpha (LAE) ont surtout été étudiées en tant que populations distinctes, sans aucune tentative de les unifier. Et il en va de même pour la LF qui est habituellement séparée entre d'un coté la fonction de luminosité UV/LBG et de l'autre celle des émetteurs \lya.


Ce manuscrit de thèse résume les trois années de travail effectuées à l'Institut de Recherche en Astrophysique et Planétologie (IRAP) sur l'étude des sources de la réionisation cosmique à l'aide d'amas de galaxies agissant comme lentille gravitationnelle et de l'instrument VLT/MUSE.
MUSE est un instrument idéal pour la détection de raies d'émission faibles, et étant un IFU, il permet une sélection complète de LAEs dans le domaine de redshift $2.9 < z < 6.7$ dans un champ de vue  de $1\arcmin \times 1\arcmin$. L'objectif principal de ce travail est de contraindre la contribution de la population des LAE à la réionisation cosmique en calculant leur LF. En utilisant les observations MUSE d'amas de galaxies agissant comme télescope gravitationnel, nous avons pu détecter et sélectionner des LAEs intrinsèquement plus faibles que dans les champs vides profonds les plus profonds. 


Comme l'analyse dans le cadre des lentilles gravitationnelles est très gourmande en terme de temps de calcul, la principale difficulté de ce projet a été de trouver une méthode efficace pour traiter les grand cubes spectroscopiques, tout en traitant chaque LAE individuellement pour tenir compte de leurs propres spécificités notamment le profil spatial et spectral ainsi que leur grandissement.

Une grande partie du travail effectué pendant cette thèse fut donc très technique et impliquait beaucoup de code pour implémenter une méthode aussi automatisée que possible pour le calcul des LFs. Ce processus a nécessité beaucoup d'essais et d'erreurs et il a fallu beaucoup plus de temps que prévu pour arriver à quelque chose de satisfaisant.


Beaucoup d'efforts ont été faits pour rendre ce code aussi automatisé que possible, afin de faciliter le processus d'ajout de futurs cubes MUSE à l'échantillon presenté dans ce travail.  Dans le cadre du ``MUSE Guaranteed Time Obsevations'' (GTO), nous n'avons pas manqué de données, mais seuls les résultats obtenus avec quatre champs (A1689, A2390, A2667, A2744) sont présentés dans cette thèse. 

Certains travaux préliminaires utilisant des champs de lentilles supplémentaires faisant partie du programme GTO ou des données disponibles auprès de collaborateurs externes ont été effectués, mais n'ont pas été inclus dans ce manuscrit faute de temps. Une nouvelle analyse utilisant un échantillon plus large fera l'objet d'une future publication en préparation.\\


En ce qui concerne la mise en page de ce manuscrit, le chapitre \ref{chap:introduction} fournit une grande introduction aux problématiques liées à la réionisation et à l'observation et la sélection des galaxies à haut redshift. Dans le chapitre \ref{chap:muse} nous présentons l'instrument VLT/MUSE, ses caractéristiques techniques  et objectifs scientifiques ainsi que la structure de ces données et la structure du bruit à l'intérieur des cubes. Dans le chapitre \ref{chap:lensing_sample}, nous décrivons l'échantillon de lentilles utilisé pour ce travail, et expliquons les bases de la lentille gravitationnelle nécessaires pour comprendre les méthodes développées dans ce travail. Le chapitre \ref{chap:3d_masks} présente la méthode adoptée pour le calcul effectif du volume ainsi que les motivations de son développement. Étant le chapitre le plus difficile et le plus technique de cette thèse, de nombreux exemples et illustrations sont fournis, et quelques lignes directrices sont données pour en faciliter la lecture.
Dans le chapitre \ref{chap:computing_lf} nous présentons les différentes étapes nécessaires pour calculer la LAE LF à partir des cubes MUSE. Ceci inclut le calcul du flux \lya et la sélection finale de l'échantillon LAE, un bref résumé du calcul du volume effectif, le calcul d'une correction de complétude et l'étape finale pour calculer la LF avec un transfert d'erreur correct. L'analyse de la LF est présentée au chapitre \ref{chap:lf_results} ainsi que la conclusion concernant la contribution de la population LAE observée à la réionisation. Enfin, dans le chapitre \ref{chap:lae_inter_lbg}, nous présentons une brève comparaison entre la population de LAE et de LBG sélectionnée dans un même volume, derrière l'amas-lentille A2744. Vous trouverez au chapitre \ref{chap:conclusion} une conclusion et un résumé de tous les travaux réalisés dans le cadre de cette thèse ainsi qu'un bref aperçu des perspectives futures possibles.




\end{otherlanguage}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
