\chapter{Intersection of the LAE and LBG populations }
\label{chap:lae_inter_lbg}

\minitoc
\listoffixmechapter
\newpage



In this chapter we investigate the interrelation between the LAE and LBG selected populations. As part of the HFF, the A2744 field has both extremely deep photometry and a large and deep MUSE coverage, allowing to make a comparison of the two populations within the same volume of Universe. Only the A2744 field is considered in this chapter. For the HFF data, we use the public source catalog released by the Astrodeep collaboration \citep{Merlin2016,Castellano2016}, which contains photometry in the 7 bands of the HST, the Hawk-I K band and Irac 1 (3.6 \si{\micro\meter}) and Irac 2 (4.5 \si{\micro\meter}). For the MUSE data, once again we use the public catalog released in \citep{Mahler2018}\footnote{\url{http://muse-vlt.eu/science/}}.\\

In Sect. \ref{sec:lae_lbg_methods} we present the method used to select both LAEs and LBGs in this FoV. This selection includes photometric redshift computation and cross matching between the Astrodeep and the MUSE source catalogs. Once a single coherent catalog is built, we study the interrelation between these two populations in Sect. \ref{sec:lae_lbg_results} with  a focus on its evolution with both redshift and luminosity. Finally in Sect. \ref{sec:lae_lbg_discussion} we briefly discuss how these results can broaden the conclusions developed in Chpt. \ref{chap:lf_results} regarding the LAEs as potential sources of reionization.


\section{Source selection}
\label{sec:lae_lbg_methods}

\subsection{Astrodeep catalog:  filtering and cross matching with MUSE detections}



% quick description of Astrodeep photometry
The complete method to determine the photometry in the Astrodeep catalog is detailed in \citet{Merlin2016}. A brief summary is given in this paragraph.
For each of the seven HST filters, both the intra-cluster light (ICL) and brightest-cluster galaxies (BCGs) were modeled and subtracted. The photometry was measured on these processed images with \sex. For Hawk-I K band and the two IRAC bands, the photometry was measured with PSF matching techniques using high spatial resolution images as prior for the source segmentation. The complete procedure is detailed in \citet{Merlin2016}. The complete catalogue has 3587 entries for the A2744 lensing field, of which 2596 are detected in the F160W image, 976 are detected in a weighted stack of F105W, F125W, F140W and F160W and undetected in F160W only, and 15 are BCGs.\\


% cleaning based on keyWords and MUSE FoV.
Before comparison with the MUSE sources, the entire Astrodeep catalogue was filtered to remove untrustworthy photometry points and/or sources. Since Hawk-I, IRAC 1 and IRAC 2 have larger PSF than the HST filters, the photometry computed in these three bands is more often contaminated by nearby galaxies. Following the flags given in the catalogue, all photometry entries likely to be contaminated (indicated by flag \verb+COVMAX+, see \citet{Merlin2016}) are shifted to lower magnitude limit. We also remove 220 sources flagged as likely residual of the foreground subtraction (\verb+SEXFLAG > 16+ and \verb+VISFLAG > 1+). Finally and because we want to compare LAEs and LBGs within a same volume of Universe,  701 entries of the catalogue that are not within the MUSE FoV are also removed. After these first steps of the selection, 2666 sources are remaining in the Astrodeep catalogue. Note that the Astrodeep catalogue is mostly based on the detection on the HST F160W image and therefore it is mainly an H-band selected sample, also including an additional sample of sources which are undetected on F160W image but appear on  stacked infrared image (Y + J + JH + H). This NIR selection is important in the subsequent discussion.\\


% cross matching
In order to build a single and coherent catalogue, a cross match is done between the remaining 2666 Astrodeep sources and the 171 LAEs of the MUSE catalogue, using a matching radius of $r = 0.8\arcsec $ (4 MUSE pixels). In case several entries are pointing toward the same sources of the opposite catalogue, only the closest one is kept. As a result of this match, 113 LAEs are successfully matched to an Astrodeep entry, and 58 are not. The difference in morphology between the UV and \lya emission as well as the difference in spatial resolution between HST and MUSE data (see Fig. \ref{fig:muse_hst_comparison}) are the dominant source of risk for making mismatches. For that reason, all LAEs and their closest Astrodeep counterpart are manually inspected to confirm or not any potential match. In the case of multiple image systems, it is always possible to select the less ambiguous image of the system when assembling the final sample.

At the end of the matching procedure, our merged catalog has 2725 entries of which 113 are seen by both MUSE and HST, and 58 are LAEs with no optical counterparts on HST images.





\subsection{SED fitting and photometric redshift with \hyperz}
\label{subsec:hyperz_run}





For all entries of the merged catalogue, we use \textsc{New$-$Hyperz}\footnote{\url{http://userpages.irap.omp.eu/\~rpello/newhyperz/}}, originally developped by\citet{Bolzonella2000}, to compute photometric redshifts and probability distributions $P(z)$ in the range $z = 0 - 8$. This method is based on the fitting of the photometric Spectral Energy Ditributions (SED) of galaxies. The template library used in this
study for these purposes includes 14 templates: 8 evolutionary 1 synthetic SEDs from the Bruzual \& Charlot code \citep{Bruzual2003}, with Chabrier IMF \citep{Chabrier2003} and solar metallicity, a set of 4 empirical SEDs compiled by \citep{Coleman1980}, and 2 starburst galaxies from the \citep{Kinney1996} library. Internal extinction is considered as a free parameter following the \citet{Calzetti2000} extinction law, with AV ranging between 0 and 1.0 magnitudes, and no prior in luminosity.\\

For sources with no spectroscopic redshift, \hyperz is run two times: the first time to determine the best photometric solution and $P(z)$, and a second time using the results of the first run to constrain the redshift within the uncertainties derived from $P(z)$ and perform Monte Carlo (MC) iterations on the photometry points. The result of this second run is used to measure a distribution of \lya continuum and UV continuum at 1\,500\si{\angstrom} from the best SEDs of each source. Finally, the blind photoz are compared to what is obtained using templates of star forming galaxies and young bursts (less than 1 Gyr) extracted from the Starbursts99 library \citep{Leitherer1999} and forcing the redshift in the $z > 2.9$ range. When comparing the blind photoz obtained during the first pass to the values derived from this final fit, they are found to be in full agreement for the selected LBGs (see Sect. \ref{subsec:selection_criteria})


For sources with spectroscopic redshift available, a first run is done without constraints to make possible the comparison between the best photometric redshift and the spectroscopic one (see Fig. \ref{fig:comp_zspec_zphot}). A second run with MC iterations on the photometry points is done, forcing the redshift at the spectroscopic value to measure a distribution of \lya and UV continuum distributions. To have a comparable magnitude of reference, an absolute UV magnitude (noted \Muv) is computed from the UV continuum measured from the best SEDs of each source.\\



The quality of photometric redshifts is assessed by directly comparing the results for the 113 galaxies with known spectroscopic redshifts in the merged catalog. The results of this comparison are shown in Fig. \ref{fig:comp_zspec_zphot}. Outliers are defined as sources with
$|z_{\rm spec} - z_{\rm phot}| >
0.15(1+z_{\rm spec})$. The accuracy reached excluding outliers is in average $\Delta z/(1 + z) = -0.011 \pm 0.053$, with median $(\Delta z/(1 + z)) = 0.001$. As seen in Fig. \ref{fig:comp_zspec_zphot}, a vast majority of galaxies with poor photometric redshifts have an apparent magnitude $m(F125W)\ge 28$ (or a S/N $<5$ in this filter). It is also worth noticing that good photometric redshifts are obtained for galaxies with $z \gtrsim 2.9 $, which mean that we are fully covering the redshift domain of the \lya emission in MUSE data.



\begin{figure}[htb!]
\centering
\includegraphics[width=0.75\hsize]{Figures/zspec_zphot_comparison.pdf}
\caption{Comparison between the spectroscopic and photometric redshift for sources for which both are available. The observed magnitude in the F125W filter is encoded in colour bar. The two series of dashed lines show the relations
  $z_{\rm phot}= z_{\rm spec} \pm 0.05\times(1 + z_{\rm spec}) $ and
  $z_{\rm phot}= z_{\rm spec} \pm 0.15\times(1 + z_{\rm spec}) $ respectively. Most of the outliers on this graph are extremely faint sources with low S/N.}
\label{fig:comp_zspec_zphot}
\end{figure}



\subsection{selection criteria}
\label{subsec:selection_criteria}


The main criterion used for the LBG selection is that a galaxy must have an integrated probability of having a redshift $z > 2.9 $ of 60\% or more (noted hereafter $P(z > 2.9) \ge 60 \%$). Given the accuracy of the photometric redshifts for galaxies with $m \gtrsim 28$ and the trend observed in Fig. \ref{fig:comp_zspec_zphot}, a S/N$ > 5 $ is also required in at least one of the seven HST filters for sources with no spectroscopic redshifts (i.e. pure LBGs).  Using this blind photometric redshift approach, 536 are selected as LBGs. \\


In addition to this blind approach, a colour-colour selection is also applied   following the prescriptions in \citet{Bouwens2015b}, to select LBGs at $z \sim $3.8, 4.9, 5.9, and 6.8. All 383 sources selected with this additional criterion are within the sample selected via blind SED-fitting, at the exception of ten of them. Manual inspection of these ten objects revealed that they either have unreliable photometric points resulting in poorly constrained or undefined photometric redshift or that they have $z_{\rm phot } \sim 2.9$, causing a large part of their $P(z)$ to be under the $z = 2.9$ and therefore failing the $P(z > 2.9) \ge 60 \%$ test.\\

Finally, the multiple images are removed from the sample. For LAEs, a robust identification of multiple systems is already provided in the MUSE catalogue. In case one image of the system matches an UV counterpart and the other(s) do not, the one with the successful mach is kept. For pure LBGs, the identification of multiple system is done using \lens and the mass models of \citet{Mahler2018}. The pure LBGs being much more numerous than the LAEs, any mis-identification of multiple systems has very low impact on the relative intersection between the LAE and LBG population.



\section{Results}
\label{sec:lae_lbg_results}

\subsection{Overview}

Once the original sample is cleaned, limited to the MUSE FoV, cross-matched with MUSE data, SED-fitted and cleaned of multiple images, the remaining sources are divided in three samples: 


\begin{itemize}
\item sample 1 for galaxies selected as both LAEs and LBGs: {\bf 92 sources}
\item sample 2 for galaxies selected as LBGs only: {\bf 408 sources}
\item sample 3 for galaxies selected as LAEs only: {\bf 46 sources} \\
\end{itemize}


The layout of these 3 samples in the A2744 FoV is shown in Fig. \ref{fig:lae_inter_lbg_view}. One of the concern was that some galaxies would be seen as pure LAEs  because their \lya emission can be detected with MUSE through some bright foreground galaxy whereas their UV continuum cannot (see Sect. \ref{sec:muse_to_study_galaxies} for a review of emission line and continuum detection with MUSE and HST). Such identification of pure LAEs would be completely artificial as it would not be representative of the intrinsic properties of the source but only of the foreground pollution. By adjusting the selection of the multiple image (when possible), we ensured that this artificial effect is not playing any important role in the final statistics of the three samples described above.  In Fig. \ref{fig:lae_inter_lbg_view}, it can be seen that none of the pure LAEs (red circles) falls on top of a bright  foreground galaxy that would prevent the detection of an underlying UV continuum.\\



For the galaxies selected as both LAE and LBG, it is possible to compute their \ewlya using their detection flux and the \lya continuum measured from the best SEDs. For galaxies selected as pure LAE, only a lower limit of \ewlya can be determined since by construction of the sample they do not have any UV detection. The \ewlya values for sample 1 and 3 are presented in Fig. \ref{fig:ew}. Since the magnification affects both the continuum and the \lya emission in the same way, no correction is needed.


\begin{figure}
\centering
\includegraphics[width=1\hsize]{Figures/A2744_lae_inter_lbg.pdf}
\caption{Color image of A2744 showing the area covered by the MUSE observations, obtained by combining the following HFF filters: F606W (blue), F814W (green) and F125W (red). The different populations selected in this field  are displayed as follows: pure LBG (cyan), LAE with LBG counterpart (yellow) and pure LAE without LBG counterpart (red). Circles are 1.5\arcsec in diameter.}
\label{fig:lae_inter_lbg_view}
\end{figure}




\begin{figure}[htb!]
\centering
\includegraphics[width=0.75\hsize]{Figures/ew.pdf}
\caption{Equivalent width of LAEs in the A2744 FoV. The pure LAEs are lower limits since by definition they do not have optical counterparts on HST images. The dashed horizontal line is \ewlya = 25 \si{\angstrom} limit.}
\label{fig:ew}
\end{figure}







\subsection{Evolution with redshift}


The three redshift histograms for the three samples considered in this work are shown stacked on top of each other in Fig. \ref{fig:z_hist}. Table \ref{tab:inter_lae_lbg} display the same information but using the redshift bins used for the LF computation in Chpt. \ref{chap:lf_results}. This figure (and Table) shows that the proportion of LAEs (sample 1 and sample 3) increases with redshift. When studying high redshift galaxies and marginalizing over magnitude and \lya luminosity, a significant part of the galaxy population is missing when looking at only the LBG or LAE population.\\

\begin{figure}
\centering
\includegraphics[width=0.75\hsize]{Figures/z_hist_3.pdf}
\caption{Redshift histogram of the three samples stacked on each other. The total sample (black line) is the sum the pure LAEs (red), the pure LBGs (grey) and the intersection of the two (yellow).}
\label{fig:z_hist}
\end{figure}




\input{Tables/lae_lbg_table.tex}



Using sample 1 and 2 it is also possible to compute the fraction of LAEs among UV selected galaxies, noted X$_{\rm LAE}$ (see Sect. \ref{subsubsec:highz_laes} for an introduction to this LAE fraction). This computation is traditionally done for LAEs with \ewlya $ > 25\si{\angstrom}$ and divided in two populations: galaxies with M$_{\rm UV} < - 20.25 $ and galaxies with M$_{\rm UV} > - 20.25 $ \citep[see e.g.][]{Stark2011,Pentericci2011,ArrabalHaro2018}. Because of the lensing nature of the sample used for this work, mostly faint galaxies are selected and 98\% of the sample falls within the  \Muv $ > - 20.25 $ domain (see e.g. Fig. \ref{fig:lum_vs_mag}). The limit \ewlya $ = 25 \si{\angstrom}$ is shown in Fig. \ref{fig:ew}. The LAE fraction is computed from sample 1 and 3 with the cuts in both \ewlya and UV magnitude described above and with no correction for completeness in the LBG or the LAE selection. The results are presented in Fig. \ref{fig:ew_fraction} using the following redshift bins: 2.9 < $z$ < 4.0, 4.0 < $z$ < 5.0, 5.0 < $z$ < 6.0 and 6.0 < z < 6.7. This binning was adopted to have enough statistics in each bins and to split the LAE population between the LAEs seen before the end of reionization at $z \gtrsim 6 $ and the ones seen after the end of reionization. Our results are compared to the results derived in \citet{Stark2011,Pentericci2011,Treu2013,Schenker2014,Tilvi2014,deBarros2017,ArrabalHaro2018}. As expected our value of X$_{\rm LAE}$ drops at $z > 6$, which can be interpreted as an increase in the neutral fraction of hydrogen. Even though all of our points are roughly 1$\sigma$ consistent with the other values, it appears that they tend to be systematically a bit lower than the previous estimates, at the exception of the points derived in \citet{ArrabalHaro2018}. Several explanations are possible and may play a role in this observed trend.

\begin{itemize}
  
\item Because of the lensing nature of our sample, the volume probed is small (only 16\,000 \si{\cubic\Mpc} are explored behind A2744 for the redshift range $2.9 < z < 6.7$) and the cosmic variance has a high impact on the observed statistics.


\item It is possible that some multiple systems are missed in sample 2. If this is indeed the case, it means that we over estimate the number of LBGs in the fields and therefore underestimate the fraction of LAEs.


\item Difference in the selection processes. In this work we use both broad band observations and IFU observations. The combination of these two methods ensures that we are as unbiased as possible in both the LBG and LAE selection in the same volume, range of magnitude and \lya luminosity explored. For example,  \citet{Stark2011} only use multi object spectroscopy to determine the prevalence of LAEs in the LBG population, but these observations are based on drop-out photometric priors \citep[see][]{Stark2010}. On the contrary, \citet{ArrabalHaro2018} only uses NB observations for the selection of the two populations and is therefore more sensitive to the intrinsically bright/high \ewlya LAEs. \\

\end{itemize}


Looking at the evolution of the two populations with redshift only, our understanding of interrelation of the two populations remains limited. On the next section we investigate the relation between UV magnitude and \lya luminosity in our three samples of interest.

\begin{figure}[htb!]
\centering
\includegraphics[width=0.75\hsize]{Figures/EW_fraction.pdf}
\caption{Fraction of LAEs with \ewlya $ > 25\si{\angstrom}$ among UV selected galaxies with \Muv $ > -20.25$. This fraction is computed from the galaxies in sample 1 and 2. A small redshift offset is applied to some of the points centred on the same redshift value.}
\label{fig:ew_fraction}
\end{figure}


\subsection{Evolution with luminosity and UV magnitude}

\begin{sidewaysfigure}
\centering
\includegraphics[width=0.9\hsize]{Figures/lum_mag.pdf}
\caption{Absolute UV magnitude versus \lya luminosity with redshift encoded in the color bar. On the top (right) are the three UV absolute magnitude (\lya luminosity) histograms stacked on each other. The sources in sample 2 have upper limit estimation of their \lya flux and the galaxies of sample 3 have lower limits on their absolute UV magnitude. All values are corrected for magnification. The thick blue line is where SFR$_{\rm Ly\alpha}$ = SFR$_{\rm UV}$. The dashed violet, pink and orange lines correspond to the fit of the of this SFR line with a free offset (i.e. free \flya / $f_{\rm uv}$ ratio) when sample 1 is split in the following three redshift bins $2.9 < z < 4.0$, $4.0 < z < 5.0$ and $5.0 < z < 6.7$. The uncertainties on these offsets are shown as vertical lines of the same color. The two vertical grey dashed lines correspond to \Muv = -17 and \Muv = -13 which are the two limits considered in \citet{Bouwens2015b}. The red and blue arrows show respectively the effect of  $\mu = 5$ and $A_v = 0.5$.} 
\label{fig:lum_vs_mag}
\end{sidewaysfigure}


% Description of the plot
The main results of this section are summed up in Fig. \ref{fig:lum_vs_mag} which shows the three samples on a $\log L_{Ly\alpha}$ versus \Muv plot where all values are corrected for magnification. In addition, luminosity histogram and absolute UV magnitude histograms are provided on the side.

Only galaxies of sample 1 have both their \lya luminosity and UV magnitude measured. For the pure LBGs of sample 2, the upper limit of their \lya emission is determined assuming a constant limit  flux of detection F$_{\rm Ly\alpha } = 0.3\times 10^{-18}$ which is then corrected for magnification and redshift. In a similar way, for the pure LAEs of sample 3, a limit monochromatic flux of $1.510^{-21}\si{\erg\per\square\cm\per\second\per\angstrom}$ is  assumed and is corrected for magnification and redshift to obtain the absolute UV magnitude. For the galaxies of sample 2 and 3, the limits derived are rough but good enough to clearly see some trends on Fig. \ref{fig:lum_vs_mag}.\\


% description Constant SFR line and explanation of what it means
In addition the equality ${\rm SFR}_{\rm Ly\alpha} = {\rm SFR}_{\rm UV}$ is also represented as a thick blue line. This relation is computed using the standard conversion in \citet{Kennicutt1998} between H$_{\alpha}$ and \lya and assuming the case B recombination \citep{Osterbrock2006} for \sfrlya, and the common conversion also given in \citet{Kennicutt1998} based on a Saltpeter IMF for the UV SFR. (See the paragraph on escape fraction in Sect. \ref{subsubsec:highz_laes} for more details). Using Eq. \ref{eq:escape_fraction}, we obtain the relation $\log L_{\rm Ly\alpha } = 34.80 - 0.4 {\rm M}_{\rm UV}$. Since none of the values in Fig. \ref{fig:lum_vs_mag} are corrected for dust absorption, this equation represents the equality between the two \textit{observed} SFRs. In other words, any galaxy falling on this line has \fuv = \flya. Regarding the assessment of the total ionizing flux density or SFRD, it means that the same ionizing emissivity is deduced when measuring either the UV continuum or the \lya line flux. The galaxies found above that line therefore have \flya > \fuv  and the ones found below have \flya < \fuv.
As already discussed in Sect. \ref{subsubsec:highz_laes}, the above equality assumes a stationary and constant regime of star formation which can explain part of the scatter observed on this figure. This aspect is discussed later in this section. \\


% discussion of the three samples + side histograms
The three samples appear clearly delimited in this plot. The first thing to notice is that for \Muv < - 17 (leftmost grey and vertical dashed line), all SFGs of our sample are selected as LBGs. However and as seen on the top histogram of the figure, when looking at galaxies fainter than \Muv = - 17, the proportion of SFGs only seen as LAEs increases. This finding is in agreement with the work of \citet{Maseda2018} which concluded that the LAE selection is better suited to study intrinsically UV faint galaxies. This also suggests that the LAE LF is a better proxy of the SFGs LF when focusing on the faint end. But once again this observed trend  depends on the relative depth achieved between MUSE and HST observations: deeper HST observations would increase the number faint LBGs detected. Needless to say that such observations are out of reach for current facilities.



% fitted lines
In addition to the ${\rm SFR}_{\rm Ly\alpha} = {\rm SFR}_{\rm UV}$  line (thick blue line in Fig. \ref{fig:lum_vs_mag}), the same relation was adjusted with a free offset to the galaxies of sample 1 divided in three redshift bins: $2.9 < z < 4.0$ (43 sources), $4.0 < z < 5.0$ (27 sources) and $5.0 < z < 6.7$ (22 sources). The results are respectively the violet, pink and orange dashed line in Fig. \ref{fig:lum_vs_mag}, and the uncertainties are represented as vertical lines of the same color. Leaving this offset free  means leaving the ratio \rf free, and boils down to measuring its average value over the sample considered. For the two lower redshift bins, the adjusted line is in agreement with \flya = \fuv within the 1$\sigma$  limit and for the third redshift bins we measure \flya > \fuv with a significance over the 1$\sigma$ limit. For all three redshift bins, a large scatter is present which is representative of individual variation of \rf that can be explained by the dust content, the geometry of the galaxy, the regime of star formation. These aspects are discussed later in Sect. \ref{sec:lae_lbg_discussion}.

When looking at all the galaxies of sample 1 together, two regimes are appearing. In average, for \Muv $\lesssim -17$ the sources tend to be under the ${\rm SFR}_{\rm Ly\alpha} = {\rm SFR}_{\rm UV}$ line, and for \Muv $\gtrsim -17$ they tend to be over this line. In addition, and following this observed trend, all the galaxies of sample 3 without exception are above that line, showing that with increasing \Muv the \lya escape fraction increases in comparison to the UV photons escape fraction. Once again, such an evolution can be explained by the evolution of the dust content, and the relative distribution of dust and stars in star-forming regions, as discussed in the next section.





\section{Possible interpretation}
\label{sec:lae_lbg_discussion}

\fxnote{add citations to this section}


% agreement with MASEDA and ARRABAL HARO
We have seen in the previous sections, that IFU observations and LAE selection are  more efficient to select intrinsically UV faint galaxies. The increasing relative number of LAEs with redshift is in average coherent with this observation, since following the hierarchical formation of galaxies theory, high redshift galaxies tend to be smaller and fainter. These two observed trends are in agreement with the conclusion and observations developed in \citet{Maseda2018}, \citet{ArrabalHaro2018} (see also Fig. \ref{fig:arrabal_haro_histo} in Introduction). It is worth noting that the LBG sample used in this thesis is NIR-selected (i.e. selected based on the rest frame continuum between $\sim$ 4\,000\si{\angstrom} and 2\,000\si{\angstrom} for $z \sim $ 3 to 7 respectively). We expect a systematic trend in the sense that LBG galaxies with extremely blue continuum could have been missed at $z \sim 3 - 4$ with respect to $z \sim 6 - 7$. However, this effect can hardly account for the systematic trends presented above.\\


% limit Muv < -17
From Fig. \ref{fig:lum_vs_mag}, we see that when only considering galaxies with \Muv < -17, all our LAEs are also selected as LBGs. This value is interesting since it roughly corresponds to observational limit of the deepest HST blank fields (see e.g. Fig. \ref{fig:uv_lf_evolution} which shows the UV LFs derived in \citet{Bouwens2015b}). Regarding the LF and reionization, and neglecting the differences between \flya and \fuv, it means that the UV LF does not need further corrections to account for the contribution of the LAE population.

% limit Muv > -17
However for \Muv > -17, the situation is less clear: as absolute UV magnitude increases, the proportion of LAEs and pure LAEs seems to be increasing. But since the \Muv estimates are only lower magnitude limits for the galaxies of samples 3 it is not possible to see whether they probe similar domains of \Muv or not. Stacking the galaxies of sample 3 in redshift bins (in a similar way as in \citet{Maseda2018}) would allow us to know more on their average UV continuum, but because of the variations of individual magnification, such a task is not straightforward. In the future, we will need to push the investigation in this direction to get more quantitative results.  As for now, the only thing that can be said is that when comparing galaxies of sample 2 and 3 with similar redshifts, it looks like there is little to no overlap in \Muv (i.e. lower limits of the yellow points of sample 2 are excluding the \Muv range probed by the yellow points of sample 2). In other words, based on the present results it is still difficult to quantify the missing contribution of LAEs to the ionizing flux density with respect to the extrapolation of the UV LF to \Muv $\sim - 13$, if any.



While this magnitude domain of \Muv $> -17 $ is mostly out of reach of deep blank fields, it is accessible through observations of strong lensing clusters. The UV LFs computed in \citet{Livermore2017,Bouwens2017,Atek2018} using lensing clusters are reasonably well constrained to the level of \Muv $\sim -15$  (see Fig. \ref{fig:uv_lf_lensing} in Introduction). For now (and neglecting the differences between \flya and \fuv)  it seems unlikely that the UV LF are requiring a significant correction to account for contribution of the LAE only population up to $z \sim 7$.\\




In the previous paragraph we discussed whether the UV LF is efficient to assess the complete numerical density of SFGs in a given volume. From this work it seems plausible that the UV selection is efficient to select all SFGs within the explored range of UV magnitude. Assuming that this is indeed the case does not mean that the UV LF allows to see all the ionizing photons since it remains affected by \fuv. And therefore to determine the sources of reionization the crucial point is to know how \fuv compares to \flya.

The LAEs (sample 1 and sample 3) with \Muv > -17 on Fig. \ref{fig:lum_vs_mag} are almost all above the \sfrlya = \sfruv line whereas the ones with \Muv < -17 tend to be under that line, suggesting that \flya > \fuv for faint UV galaxies and \flya < \fuv for UV "bright" galaxies. The ratio \rf is also increasing with redshift as shown by the orange dashed adjusting the \rf ratio of the galaxies of sample 1 in the redshift range $5.0 < z < 6.7$. This evolutionary trend, taken at face values, suggests that the \lya emission is better suited for the study of high redshift UV faint ionizing sources, since it appears that we tend to recover more of their ionizing photons.


% preferential escape of lya photons : geometry
One possibility suggested by \citet{Atek2014} is that multi-phase ISM with dust can produce an enhancement of the \lya emission  with respect to the non resonant emission at similar wavelength (i.e. Lyman continuum photons, or UV photons in our case). Following this scenario, neutral gas and dust reside in clumps surrounded by an ionized medium. The \lya photons are scattered when reaching the surface of these clumps while the UV photons can penetrate inside and are more easily absorbed by the dust. Therefore even though \flya decreases with increasing dust content or reddenning \citep[see e.g.][]{Hayes2011,Atek2014,Matthee2016}, the ratio \rf can increase since \fuv decreases faster than \flya in presence of these dusty clumps. In addition, in galaxies observed edge-on, the \lya photons are able to scatter and go around the dust in the dust barrier while the UV is strongly attenuated when passing directly through it. This can explain part of the scatter and the UV faint LAEs of sample 3 that are far above the blue line.

% opposit in UV bright as seen here
Finally \citet{Atek2014} observes a decreases fo \sfrlya with increasing \sfruv, which can be interpreted as a decrease of \flya with increasing UV luminosity. This trend is also predicted by models \citep[see e.g. the model of][]{Garel2012} and can be explained by the older stellar population in UV bright galaxies. They are more dusty and less clumpy than galaxies with younger stellar population or star bursts. The more uniform dust distribution makes the absorption of \lya more likely and can cause \fuv > \flya. This is a possible interpretation for the galaxies of sample 1 with \Muv < -17 that are observed preferentially under the \sfruv = \sfrlya line in Fig. \ref{fig:lum_vs_mag}.



But all the interpretations described above have to be moderated by the fact that the relation \sfrlya = \sfruv is only valid under the assumption of constant star formation rate as presented in Sect. \ref{subsubsec:highz_laes}. When this is not  the case, the \lya emission is no longer representative of the actual SFR owing to the long escape time scale of \lya radiation. When a recent star burst fades away, the UV emission turns off immediately while the \lya continues at the same rate since all \lya photons emitted during the star burst are still going through a lengthy radiative transfer in the galaxy halo. Such process could explain why the galaxies of sample 3 are orders of magnitude over the blue line.\\



% Evolution with redshift

Following the points given in the previous paragraph, the increase of \rf with redshift (measured by the evolution of the offset on the purple pink and orange dashed lines) is likely to trace the evolution of the dust distribution. As shown in \citet{Hayes2011}, the amount of dust decreases with increasing redshift, as the stellar population become younger. Following this simple evolution and assuming a homogeneous distribution of dust, we would expect a similar increase in both \flya and \fuv. This is not what is observed, which leaves an increased clumpiness in the distribution of dust and neutral hydrogen at higher redshift to explain the observed evolution of \rf.






























%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
